create or replace view PRD_MDL_NOSHOW_OTPT_FACT as
 (
 SELECT 
 source_nm,
 PRED_MODEL_FACT.subject_id as MRN,
 json_value(PRED_MODEL_FACT.subject_json, '$.Obs') as Obs,
 NVL(json_value(PRED_MODEL_FACT.subject_json, '$.clinic'), 
 json_value(PRED_MODEL_FACT.subject_json, '$.FHPClinicNum')) as clinic,
 json_value(PRED_MODEL_FACT.subject_json, '$.PTLNAME') as PTLNAME,
 json_value(PRED_MODEL_FACT.subject_json, '$.PTFNAME') as PTFNAME,
 json_value(PRED_MODEL_FACT.subject_json, '$.incident') as incident,
 json_value(PRED_MODEL_FACT.subject_json, '$.prob1') as prob1,
 json_value(PRED_MODEL_FACT.subject_json, '$.prob2') as prob2,
 json_value(PRED_MODEL_FACT.subject_json, '$.prob3') as prob3,
 json_value(PRED_MODEL_FACT.subject_json, '$.prob4') as prob4,
 json_value(PRED_MODEL_FACT.subject_json, '$.prob5') as prob5,
 json_value(PRED_MODEL_FACT.subject_json, '$.avg_change') as avg_change,
 json_value(PRED_MODEL_FACT.subject_json, '$.curr_unoshow') as curr_unoshow,
 json_value(reason_codes1.subject_json, '$.desc') as reason_desc1,
 json_value(reason_codes2.subject_json, '$.desc') as reason_desc2,
 json_value(reason_codes3.subject_json, '$.desc') as reason_desc3,
 json_value(reason_codes4.subject_json, '$.desc') as reason_desc4,
 json_value(reason_codes5.subject_json, '$.desc') as reason_desc5,
 "PRED_MODEL_FACT"."INSERT_DATE" AS "INSERT_DATE",
 "PRED_MODEL_FACT"."INSERT_BY" AS "INSERT_BY"

FROM "BI_SRC"."PRED_MODEL_FACT" "PRED_MODEL_FACT"
 inner join PRED_MODEL_SOURCE_DIM 
 on PRED_MODEL_FACT.SOURCE_ID = PRED_MODEL_SOURCE_DIM .SOURCE_ID
 left join "BI_SRC"."PRED_MODEL_FACT" reason_codes1
 on json_value(PRED_MODEL_FACT.subject_json, '$.top1_riskfactor') = reason_codes1.subject_id and reason_codes1.source_id = 4
 left join "BI_SRC"."PRED_MODEL_FACT" reason_codes2
 on json_value(PRED_MODEL_FACT.subject_json, '$.top2_riskfactor') = reason_codes2.subject_id and reason_codes2.source_id = 4
 left join "BI_SRC"."PRED_MODEL_FACT" reason_codes3
 on json_value(PRED_MODEL_FACT.subject_json, '$.top3_riskfactor') = reason_codes3.subject_id and reason_codes3.source_id = 4
 left join "BI_SRC"."PRED_MODEL_FACT" reason_codes4
 on json_value(PRED_MODEL_FACT.subject_json, '$.top4_riskfactor') = reason_codes4.subject_id and reason_codes4.source_id = 4
 left join "BI_SRC"."PRED_MODEL_FACT" reason_codes5
 on json_value(PRED_MODEL_FACT.subject_json, '$.top5_riskfactor') = reason_codes5.subject_id and reason_codes5.source_id = 4
 where PRED_MODEL_FACT.source_id in (9)
 --and "PRED_MODEL_FACT"."INSERT_DATE" > (select max("PRED_MODEL_FACT"."INSERT_DATE") from "PRED_MODEL_FACT" where PRED_MODEL_FACT.source_id in (2,3)) - 1
 )
