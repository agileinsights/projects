create view BI_SRC.DIAL_TX_WT_30DAY as
select 
DIAL_TX_ID,
DIAL_ATTR_DIM_ID,
DIAL_EVAL_ATTR_DIM_ID,
DIAL_MACH_ATTR_DIM_ID,
fct.pt_dim_id,  
fct.pre_wgt_num - fct.pre_available_wgt_num as EDW,
fct.pre_available_wgt_num,
fct.pre_wgt_num,
fct.pre_wgt_gain_num,
fct.post_wgt_num,
fct.post_wgt_chng_num,
fct.tot_mach_mn_num ,
fct.post_wgt_num
  - (fct.pre_wgt_num + (fct.pre_available_wgt_num * -1))
  post_weight_variance,
 cASE
     WHEN (    fct.tot_mach_mn_num IS NOT NULL
     AND post_wgt_num IS NOT NULL
     AND fct.tot_mach_mn_num <> 0
     AND post_wgt_num <> 0)
     THEN
         (fct.pre_wgt_num / fct.post_wgt_num - 1)  / fct.tot_mach_mn_num  * 1000 * 60
     ELSE
          NULL
     END ufr, 
LOC.DL_ID,
fct.mach_on_dtm,
PAT.FMC_MED_REC_NBR mrn 
from  
    CS_CLINICAL.DIAL_TX_FACT_H fct,
    CONFORMED_DIMS.PT_DIM pat,
    CONFORMED_DIMS.DIAL_LOC_DIM loc
    where  pat.pt_dim_id = fct.pt_dim_id
    AND loc.dl_dim_id = fct.dl_dim_id
    and fct.mach_on_dtm > current_date - 30