create or replace view PRD_MDL_HOSP_OTPT_FACT as
 (
 SELECT 
 source_nm,
 PRED_MODEL_FACT.subject_id as MRN,
 json_value(PRED_MODEL_FACT.subject_json, '$.Obs') as Obs,
 NVL(json_value(PRED_MODEL_FACT.subject_json, '$.clinic'), 
 json_value(PRED_MODEL_FACT.subject_json, '$.FHPClinicNum')) as clinic,
 json_value(PRED_MODEL_FACT.subject_json, '$.PTLNAME') as PTLNAME,
 json_value(PRED_MODEL_FACT.subject_json, '$.PTFNAME') as PTFNAME,
 json_value(PRED_MODEL_FACT.subject_json, '$.FMS_CLINICAL_MANAGER') as FMS_CLINICAL_MANAGER,
 json_value(PRED_MODEL_FACT.subject_json, '$.FM_REGIONAL_VP') as FMS_REGIONAL_VP,
 json_value(PRED_MODEL_FACT.subject_json, '$.DIR_OPS') as DIR_OPS,
 json_value(PRED_MODEL_FACT.subject_json, '$.FMS_DIETICIAN') as FMS_DIETICIAN,
 json_value(PRED_MODEL_FACT.subject_json, '$.FMS_SOCIAL_WORKER') as FMS_SOCIAL_WORKER,
 json_value(PRED_MODEL_FACT.subject_json, '$.LEAD_SOCIAL_WORKER') as LEAD_SOCIAL_WORKER,
 json_value(PRED_MODEL_FACT.subject_json, '$.LEAD_DIETICIAN') as LEAD_DIETICIAN,
 json_value(PRED_MODEL_FACT.subject_json, '$.RISK_LEVEL_Q42016') as RISK_LEVEL_CRNT_QTR,
 json_value(PRED_MODEL_FACT.subject_json, '$.HIGH_MEDIUM_FROM_Q3') as HIGH_MEDIUM_PRVS_QTR,
 "PRED_MODEL_FACT"."INSERT_DATE" AS "INSERT_DATE",
 "PRED_MODEL_FACT"."INSERT_BY" AS "INSERT_BY"

FROM "BI_SRC"."PRED_MODEL_FACT" "PRED_MODEL_FACT"
 inner join PRED_MODEL_SOURCE_DIM 
 on PRED_MODEL_FACT.SOURCE_ID = PRED_MODEL_SOURCE_DIM.SOURCE_ID
 where PRED_MODEL_FACT.source_id in (10)
  )
