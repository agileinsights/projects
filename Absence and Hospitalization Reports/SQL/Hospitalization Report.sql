select facility_id, trunc(admission_date, 'MONTH') as admission_month, count(patient_id) admissions
 from KC.admission
 where admission_date >= TRUNC(TRUNC(sysdate, 'MONTH') - 1, 'MONTH')
 and admission_date < TRUNC(sysdate, 'MONTH')
 group by facility_id, trunc(admission_date, 'MONTH')