select facility_id, trunc(admission_date, 'MONTH') as admission_month, count(patient_id) admissions
 from KC.admission
 where admission_date >= trunc(current_date, 'MONTH') - 150   
 and admission_date < trunc(current_date)
 and (facility_id = '1399' or facility_id = '1680' or facility_id='1767'or facility_id= '2458'or facility_id= '6004'or facility_id= '7549')
 group by facility_id, trunc(admission_date, 'MONTH')
 order by facility_id, admission_month