
 select cast(PATIENT_ID as varchar(50)) PATIENT_ID, FACILITY_ID, r.reason_description, TEMPORARY_ABSENCE_DATE, 
 max(TEMPORARY_ABSENCE_END_DATE) as TEMPORARY_ABSENCE_END_DATE 
 from kc.temporary_absence a left join kc.reason r on a.temporary_absence_reason = r.reason_code 
 where temporary_absence_date >= current_date - 30 
 and temporary_absence_date <= current_date  
 and (facility_id = '1767' or facility_id='1399'or facility_id= '7549')
 group by cast(PATIENT_ID as varchar(50)), FACILITY_ID, r.reason_description, TEMPORARY_ABSENCE_DATE 
 order by cast(PATIENT_ID as varchar(50)), TEMPORARY_ABSENCE_DATE