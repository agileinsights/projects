
 select FACILITY_ID, TRUNC(TEMPORARY_ABSENCE_DATE, 'MONTH') AS TEMPORARY_ABSENCE_MONTH, count(patient_id) admissions
--, max(TEMPORARY_ABSENCE_END_DATE) as TEMPORARY_ABSENCE_END_DATE 
 from kc.temporary_absence a left join kc.reason r on a.temporary_absence_reason = r.reason_code 
 where temporary_absence_date >= TRUNC(sysdate-130 , 'MONTH') 
 and temporary_absence_date <= TRUNC(sysdate, 'MONTH')  
 and (facility_id = '1399' or facility_id = '1680' or facility_id='1767'or facility_id= '2458'or facility_id= '6004'or facility_id= '7549')
 and    (
--        r.reason_description = 'HOSPITALIZATION' 
--        or 
        r.reason_description = 'HOSPITALIZATION (>24 HRS)' 
--        or r.reason_description = 'OBSERVATION HOSP. ADMISSION (<24 HRS)' 
--        or r.reason_description = 'TEMP TRANSFERRED TO A BMA FAC'
--        or r.reason_description = 'TEMP TRANSFERRED TO A NON-BMA FAC'
        )
 group by FACILITY_ID, TRUNC(TEMPORARY_ABSENCE_DATE, 'MONTH')
 order by facility_id, TRUNC(TEMPORARY_ABSENCE_DATE, 'MONTH')