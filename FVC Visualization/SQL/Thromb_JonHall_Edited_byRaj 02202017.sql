--USE [NGprod]
--GO
--/****** Object:  StoredProcedure [dbo].[aac_ir_thromb_imd_tots_prov_rpt]    Script Date: 2/20/2017 6:09:53 PM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER OFF
--GO
--ALTER PROCEDURE [dbo].[aac_ir_thromb_imd_tots_prov_rpt]
--       @start_date DATETIME,
--       @end_date DATETIME,
--       --@location_id VARCHAR(36),
--       @provider_id VARCHAR(36),
--       @FistulaOrGraft VARCHAR(1)
--AS
--BEGIN
--       SET NOCOUNT ON;
--/*
--   Developer:  Jon C. Hall
--   Date: 01/12/2017
--   Description:      Provide number of Immediate Success Thrombectomies in which the patient was
--                           discharged with successful restoration of flow in their primary access;
--                           whilst also providing the number of Immediate "Un"successful Thrombectomies.
--                           Results may be filtered by Fistula, Graft, or Both.
--   Input parms:      Provider ID, fistula vs. graft (or 'B' for both), and date range may be customized by user.
--*/
--       DECLARE @enterprise_id VARCHAR(5) = '00001',
--              @ss_type_1 VARCHAR(16) = '',
--              @ss_type_2 VARCHAR(16) = '',
--              @ThrombSuccessCnt INTEGER = 0,
--              @ThrombUnsuccessCnt INTEGER = 0
--       ;
--       SET @ss_type_1 = '%fistula%';     --Default both to fistula 'F'
--       SET @ss_type_2 = '%fistula%';     --Default both to fistula 'F'
--       IF @FistulaOrGraft = 'B'   --Both
--              SET @ss_type_2 = '%graft%';    
--       ELSE IF @FistulaOrGraft = 'G'
--       BEGIN
--              SET @ss_type_1 = '%graft%';
--              SET @ss_type_2 = '%graft%';
--       END

--
--SELECT @ThrombSuccessCnt = COUNT(txt_pt_name)
--       FROM
--       (
       SELECT 	
	           ap.txt_pt_name
	           , ap.txt_proc_sum
                        , ap.txt_indication_sum
                        , ap.txt_indication_list, ap.txt_site, ep.procedure_date
                        , ep.actual_procedure, ap.radio_pt_type, pd.radio_access_use,
                   MAX(ep.txt_follow_up_comp), po.txt_disc_condition,
                   ap.person_id, ap.enc_id
              FROM AAC_PHY_A ap 
              INNER JOIN patient_encounter_A pe  ON ap.enc_id = pe.enc_id
              INNER JOIN AAC_Procedure_ExtPop_A ep ON ap.enc_id = ep.enc_id
              INNER JOIN person_A per ON ep.person_id = per.person_id
              INNER JOIN AAC_POST_A po ON ap.enc_id = po.enc_id
              INNER JOIN AAC_POST_DC_A pd ON ap.enc_id = pd.enc_id
              WHERE 
--              CONVERT(VARCHAR(36), pe.rendering_provider_id) = '9338ECEC-97FE-442B-8092-465C80554517'
--                     AND 
                     ap.enterprise_id = '00001'
                     AND ep.procedure_date BETWEEN '20150101' AND '20161231'
--                   AND per.last_name NOT IN ('Test', 'Test1')      --EXCLUDE Patient Test!!!
                     AND (COALESCE(ap.radio_pt_type,'') = '1')
                     AND COALESCE(ap.txt_indication_sum, '') LIKE '%clot%'
                     AND COALESCE(ap.txt_proc_sum, '') LIKE '%Thrombectomy%'
                     AND (COALESCE(ap.txt_site, '') LIKE 'Left Upper Arm AV Graft'
                           OR
                           COALESCE(ap.txt_site, '') LIKE 'Right Upper Arm AV Graft'
                           )
		           --Raj: how about other txt_site fields?
--                     AND (ep.actual_procedure LIKE '%declot%' --Raj: This condition returns empty results
--                                  OR ep.actual_procedure LIKE '%Manually Dictated Note%') 
--                     AND CHARINDEX('non maturing fistula',ap.txt_indication_sum) = 0      --Do not want to consider these per Nancy M.
                     AND 
--                     (
                     pd.radio_access_use = 1      --Ready for Use
--                                  OR pd.radio_access_use = 2 --Wait until
--                                  OR pd.radio_access_use = 3) --Not ready for use at this time.
                                     --radio button of 4 was added for failures (Failed thrombectomy.  Do not use.) which allowed us to add 2 into the group above (which wasn't there orig).
--                     --Lisa P 4/29/13 email declared that MD choice (of radio_access_use) is only criteria needed
--                     --w/ RN choice of txt_disc_condition not needing 2B considered.
                     --AND (po.txt_disc_condition like '%strong thrill%'
                     --            OR po.txt_disc_condition LIKE '%diminished thrill%'
                     --            OR po.txt_disc_condition LIKE '%positive bruit%')
                     GROUP BY ap.person_id, ap.enc_id, ap.txt_proc_sum, ap.txt_indication_sum,
                     ap.txt_indication_list,    
                     ap.txt_site, ap.txt_pt_name,
                     ep.procedure_date, ep.actual_procedure, ap.radio_pt_type, pd.radio_access_use,
--                     ep.txt_follow_up_comp, MAX in select above allows us to take this outa GROUP BY & thereby elim dupes from bad workflows..
                     po.txt_disc_condition
                     --ORDER BY ap.person_id, ep.procedure_date      --Order By not allowd in subqry...
--       ) AS result_qry1 

--SELECT @ThrombUnsuccessCnt = COUNT(txt_pt_name)
--       FROM
--       (
              SELECT 
              ap.txt_pt_name
              , ap.txt_proc_sum, ap.txt_indication_sum,
                   ap.txt_indication_list, ap.txt_site, ep.procedure_date,
                   ep.actual_procedure, ap.radio_pt_type, pd.radio_access_use,
                   MAX(ep.txt_follow_up_comp), po.txt_disc_condition,
                   ap.person_id, ap.enc_id
              FROM AAC_PHY_A ap 
              INNER JOIN patient_encounter_A pe ON ap.enc_id = pe.enc_id
              INNER JOIN AAC_Procedure_ExtPop_A ep ON ap.enc_id = ep.enc_id
              INNER JOIN person_A per ON ep.person_id = per.person_id
              INNER JOIN AAC_POST_A po ON ap.enc_id = po.enc_id
              INNER JOIN AAC_POST_DC_A pd ON ap.enc_id = pd.enc_id
              WHERE 
--                     CONVERT(VARCHAR(36), pe.rendering_provider_id) = '9338ECEC-97FE-442B-8092-465C80554517'
--                     AND 
                     ap.enterprise_id = '00001'
                     AND ep.procedure_date BETWEEN '20150101' AND '20161231'
--                     AND per.last_name NOT IN ('Test', 'Test1')      --EXCLUDE Patient Test!!!
                     AND (COALESCE(ap.radio_pt_type,'') = '1')
                     AND COALESCE(ap.txt_indication_sum, '') LIKE '%clot%'
                     AND COALESCE(ap.txt_proc_sum, '') LIKE '%Thrombectomy%'
                     AND (COALESCE(ap.txt_site, '') LIKE 'Left Upper Arm AV Graft'
                           OR
                           COALESCE(ap.txt_site, '') LIKE 'Right Upper Arm AV Graft'
                           --Raj: how about other txt_site fields?
                           )
--                     AND (ep.actual_procedure LIKE '%declot%' --Raj: This condition returns empty results
--                                  OR ep.actual_procedure LIKE '%Manually Dictated Note%')
--                     AND CHARINDEX('non maturing fistula',ap.txt_indication_sum) = 0      --Do not want to consider these per Nancy M.
                     AND pd.radio_access_use = 4 --Failed thrombectomy.  Do not use.
                     GROUP BY ap.person_id, ap.enc_id, ap.txt_proc_sum, ap.txt_indication_sum,
                     ap.txt_indication_list,    ap.txt_site, ap.txt_pt_name,
                     ep.procedure_date, ep.actual_procedure, ap.radio_pt_type, pd.radio_access_use,
                     ----ep.txt_follow_up_comp, MAX in select above allows us to take this outa GROUP BY & thereby elim dupes from bad workflows..
                     po.txt_disc_condition
                     --ORDER BY ap.person_id, ep.procedure_date      --Order By not allowd in subqry...
--       ) AS result_qry2
--       --Manually produce results for SQL2005 SSRS pkg
--       SELECT @ThrombSuccessCnt AS ThrombSuccessCnt,
--              @ThrombUnsuccessCnt AS ThrombUnsuccessCnt
--       ;
--END