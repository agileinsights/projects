CREATE TABLE Patientportal_Wkly_Smry (
        WEEK_BEGIN_DT DATE not null,	
        CLINIC_ID varchar2(10) not null, 	
        MODALITY varchar2(200) not null, 	
        PATIENT_OID NUMBER not null, 
        MRN varchar2(30) not null, 
        REGISTRATION_ID varchar2(30) not null, 	
        IS_ENROLLED_IND NUMBER null,	
        SUBMIT_COUNT NUMBER null,	
        SCHEDULE_DAYS NUMBER null,	
        INSERTED_DT DATE null,	
        NOT_ENROLLED_IND NUMBER null,	
        NOT_ENROLLED_REASON varchar2(2000) null,	
        STOP_ENROLL_IND NUMBER null,	
        STOP_ENROLL_REASON varchar2(2000) null,	
        VITAL_ALERT_IND NUMBER null,	
        TREATMENT_ALERT_IND NUMBER null,	
        HIGH_NEG_UF_ALERT_IND NUMBER null,	
        Identifier NUMBER null,	
        CREATED_DT DATE null,
        LOGIN_DT DATE null,	
        REGISTER_DT DATE null,	
        IS_REGISTERED_IND NUMBER null,		
        IS_ENR_REG_IND NUMBER null,		
        not_enrolled_reason_other varchar2(2000) null,	
        MAX_WEEK_DT DATE null);
        
        

