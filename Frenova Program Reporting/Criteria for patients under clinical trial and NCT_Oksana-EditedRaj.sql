

SELECT 
HPV.EntityAbb as  ClinicId,
HPV.EntityName as ClinicName,
HPV.ObjectID AS PVID,
HPV.Patient_oid,
(CASE WHEN (HPatient.GenerationQualifier IS NULL OR HPatient.GenerationQualifier = '') THEN 
(CASE WHEN (HPatient.MiddleName IS NULL OR HPatient.MiddleName = '') THEN 
HPatient.LastName + ', '+ HPatient.FirstName
ELSE 
HPatient.LastName + ', ' + HPatient.FirstName + ' ' + Left(HPatient.MiddleName,1) 
END)
ELSE 
(CASE WHEN (HPatient.MiddleName IS NULL OR HPatient.MiddleName = '') THEN 
HPatient.LastName + ' ' + HPatient.GenerationQualifier + ', '+ HPatient.FirstName
ELSE 
HPatient.LastName + ' ' + HPatient.GenerationQualifier + ', ' + HPatient.FirstName + ' ' + Left(HPatient.MiddleName,1) 
END)
END) AS PatientName,
hpatientidentifiers.value AS MRN,
cast (HPerson.BirthDate as DATE) AS DOB,
cast(HPV.VisitStartDateTime as DATE) as AdmissionDate,
--HPV.EntityName ClinicName,
--HPV.EntityAbb ClinicID,
HAssessment.CollectedDT, HAssessment.AssessmentID,
max(case when HObservation.FindingAbbr = 'CT_IDE' then HObservation.Value else NULL end) as CT_IDE,
max(case when HObservation.FindingAbbr = 'CT_Investigator' then HObservation.Value else NULL end) as CT_Investigator,
max(case when HObservation.FindingAbbr = 'CT_NCT' then HObservation.Value else NULL end) as CT_NCT,
max(case when HObservation.FindingAbbr = 'CT_StudyDur' then HObservation.Value else NULL end) as CT_StudyDur,
max(case when HObservation.FindingAbbr = 'CT_StudyTitle' then HObservation.Value else NULL end) as CT_StudyTitle,
max(case when HObservation.FindingAbbr = 'CT_TherArea' then HObservation.Value else NULL end) as CT_TherArea,
max(case when HObservation.FindingAbbr = 'CT_TherOther' then HObservation.Value else NULL end) as CT_TherOther
FROM EWS_ECCB.dbo.HPatientVisit as HPV WITH (NOLOCK)
INNER JOIN EWS_ECCB.dbo.HPatient HPatient WITH (NOLOCK) 
                                  ON HPV.Patient_oid = HPatient.ObjectID 
                                 AND HPatient.IsDeleted = 0
INNER JOIN EWS_ECCB.dbo.HPatientIdentifiers HPatientIdentifiers WITH (NOLOCK) 
                                  ON HPV.Patient_oid = HPatientIdentifiers.Patient_oid
                                 AND HPatientIdentifiers.TYpe = 'MR' 
                                 AND HPatientIdentifiers.IsDeleted = 0
                                 AND HPatientIdentifiers.ValidOn is not NULL
INNER JOIN EWS_ECCB.dbo.HPerson HPerson WITH (NOLOCK) 
                                  ON HPV.Patient_oid = HPerson.ObjectID 
                                 AND HPerson.IsDeleted = 0                                 
INNER JOIN EWS_ECCB.dbo.HAssessment HAssessment with (nolock) ON HAssessment.PatientVisit_oid = HPV.ObjectID
                                    and HAssessment.Patient_oid = HPV.Patient_oid
INNER JOIN EWS_ECCB.dbo.HObservation_Active HObservation with (nolock) ON HObservation.AssessmentID = HAssessment.AssessmentID                                 
                                                         and HObservation.Patient_oid = HAssessment.Patient_oid

WHERE 
/* filter for Patients that are Active */
(HPV.VisitENDDateTime IS NULL OR HPV.VisitENDDateTime > = cast(GETDATE() as date)  )
    AND HPV.VisitStartDateTime < DATEADD(D,1, cast(GETDATE() as date) ) 
AND HPV.IsDeleted = 0
--AND HPV.EntityAbb = '1271'
AND HObservation.FindingAbbr in (
'CT_IDE',
'CT_Investigator',
'CT_NCT',
'CT_StudyDur',
'CT_StudyTitle',
'CT_TherArea',
'CT_TherOther')
AND HObservation.ObservationStatus = 'AV'
AND HAssessment.EndDt IS NULL 
AND HAssessment.AssessmentStatusCode in (1,3)
AND HAssessment.FormUsage = 'Clinical Trial Participant'
group by
HPV.EntityAbb,
HPV.EntityName,
HPV.ObjectID,
HPV.Patient_oid,
HPatient.GenerationQualifier,HPatient.MiddleName,
HPatient.LastName, HPatient.FirstName,
hpatientidentifiers.value, HPerson.BirthDate,HPV.VisitStartDateTime,
HAssessment.CollectedDT, HAssessment.AssessmentID
order by HPV.Patient_oid, HAssessment.AssessmentID
