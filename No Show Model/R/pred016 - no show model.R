library(data.table)
library(bit64)
library(gam)

load("/home/rapp/R/pred016/unoshow.RData")
data1 <- fread("/home/rapp/R/pred016/sasout.csv")
#load("unoshow.RData")
#data1 <- fread("sasout.csv")
setnames(data1,tolower(names(data1)))


data1[,patient_gender:=gsub(" ","",patient_gender)]
data1[patient_gender=="",patient_gender:=NA]
data1[,mar_status:=gsub(" ","",mar_status)]
data1[mar_status=='',mar_status:=NA]
data1[,race:= gsub(" ","",race)]
data1[race=='',race:=NA]
data1[,urbanicity:= gsub(" ","",urbanicity)]
data1[urbanicity=='',urbanicity:=NA]
data1[,pr1wk_transport1:=gsub(" ","",pr1wk_transport1)]
data1[pr1wk_transport1=='',pr1wk_transport1:=NA]
data1[,season:=gsub(" ","",season)]
data1[season=='',season:=NA]

data1[,race:=ifelse(race=="OTHER", NA, race)]

## 1st prediction run
p <- matrix(NA, nrow(data1),5)
p[,1] <- predict(unoshow_m1, newdata=data1, type="response")
p[,2] <- predict(unoshow_m2, newdata=data1, type="response")
p[,3] <- predict(unoshow_m3, newdata=data1, type="response")
p[,4] <- predict(unoshow_m4, newdata=data1, type="response")
p[,5] <- predict(unoshow_m5, newdata=data1, type="response")

####
findp <- function(x) x[which.min(is.na(x))]
pred <- apply(p, 1, findp)
out_unsorted <- data.table(patient_id=data1$patient_id,
                  facility_id=data1$facility_id,
                  prior1week_unoshow=data1$pr1wk_n_noshow,
                  probability=pred)


#out=out_unsorted
#setorder(out,-probability)

unoshow_pcnt<-sum(data1$pr1wk_n_noshow>0, na.rm=T)/sum(!is.na(data1$pr1wk_n_noshow))
out_unsorted[,unoshow:=F]
if (unoshow_pcnt>0) {
  out_unsorted[order(out_unsorted$probability,decreasing=T)[1:(round(nrow(out_unsorted)*unoshow_pcnt)-1)],unoshow:=T]
} 
sum(!is.na(out_unsorted$unoshow))


#n_true_pt <- 5 #sum(out$unoshow)


## 2nd  prediction run: take median, by pt
model_list <- list(model1=unoshow_m1, model2=unoshow_m2, model3=unoshow_m3, model4=unoshow_m4, model5=unoshow_m5)
para_list <- list(
  model1=c(
            "comorbid_anemias",
            "comorbid_cancer_ot_skin", "comorbid_chf",
            "comorbid_copd", "comorbid_diabetes",
            "comorbid_drug_alch_depedence", "comorbid_hepatitis",
            "comorbid_hiv_aids", "comorbid_mi",
            "comorbid_pad_pvd", "comorbid_count",   
            "patient_gender", "mar_status",
            "age", "vintage",
            "noshow_week", "yn_dob",
            "pr1wk_idh", "pr1wk_no_unusual_findings",
            "pr1wk_n_compl", "pr1wk_shift",
            "pr4wk_preweight_slopedaily", "pr4wk_idwg_slopedaily",
            "pr4wk_albumin", "pr4wk_nlr",
            "pr4wk_orderchange", 
            "pr1yr_csytd",
            "pr1yr_techytd", "pr1yr_ttytd",
            "pr1yr_adytd", "pr1yr_retent_neg",
            "pr1yr_n_noshow", "pr1yr_exp_days",
            "pr1wk_transport1", "pr1wk_access_cath",
            "pr1wk_prcp", "pr1wk_snow", 
            "pr1wk_tmax", "pr1wk_wt_combined", 
            "season",
            "yn_spt_event", 
            "lscore1", "lscore6",
            "lscore7", "lscore8",
            "lscore9", "lscore12",
            "lscore13", "lscore14",
            "lscore15", "lscore16",
            "lscore17", "lscore21",
            "lscore22", "lscore23",
            "urbanicity",
            
            "pr1yr_rnytd", "race", "lscore24", "lscore27"
          ),
  model2=c(
            "pr1yr_n_noshow",
            "comorbid_anemias",
            "comorbid_cancer_ot_skin", "comorbid_chf",
            "comorbid_copd", "comorbid_diabetes",
            "comorbid_drug_alch_depedence", "comorbid_hepatitis",
            "comorbid_hiv_aids", "comorbid_mi",
            "comorbid_pad_pvd", "comorbid_count",   
            "patient_gender", "mar_status",
            "age", "vintage",
            "noshow_week", "yn_dob",
            "pr1wk_idh", "pr1wk_no_unusual_findings",
            "pr1wk_n_compl", "pr1wk_shift",
            "pr4wk_preweight_slopedaily", "pr4wk_idwg_slopedaily",
            "pr4wk_albumin", "pr4wk_nlr",
            "pr4wk_orderchange", 
            "pr1yr_csytd",
            "pr1yr_techytd", "pr1yr_ttytd",
            "pr1yr_adytd", "pr1yr_retent_neg",
            "pr1yr_exp_days",
            "pr1wk_transport1", "pr1wk_access_cath",
            "season",
            "yn_spt_event", 
            "lscore1", "lscore6",
            "lscore7", "lscore8",
            "lscore9", "lscore12",
            "lscore13", "lscore14",
            "lscore15", "lscore16",
            "lscore17", "lscore21",
            "lscore22", "lscore23",
            "urbanicity",
            "pr1yr_rnytd", "race", "lscore24", "lscore27"
          ),
  
  model3=c(
            "pr1yr_n_noshow",
            "comorbid_anemias",
            "comorbid_cancer_ot_skin", "comorbid_chf",
            "comorbid_copd", "comorbid_diabetes",
            "comorbid_drug_alch_depedence", "comorbid_hepatitis",
            "comorbid_hiv_aids", "comorbid_mi",
            "comorbid_pad_pvd", "comorbid_count",   
            "patient_gender", "mar_status",
            "age", "vintage",
            "noshow_week", "yn_dob",
            "pr1wk_idh", "pr1wk_no_unusual_findings",
            "pr1wk_n_compl", "pr1wk_shift",
            "pr4wk_preweight_slopedaily", "pr4wk_idwg_slopedaily",
            "pr4wk_albumin", 
            "pr4wk_orderchange", 
            "pr1yr_exp_days",
            "pr1wk_access_cath",
            "season", "yn_spt_event", "race"
          ),
  model4=c(
          "pr1yr_n_noshow",
          "comorbid_anemias",
          "comorbid_cancer_ot_skin", "comorbid_chf",
          "comorbid_copd", "comorbid_diabetes",
          "comorbid_drug_alch_depedence", "comorbid_hepatitis",
          "comorbid_hiv_aids", "comorbid_mi",
          "comorbid_pad_pvd", "comorbid_count",
          "yn_dob", "season"
  ),

  model5=c(
          "event_unoshow", 
          "pr1yr_n_noshow"
  )
)

merged_data <- cbind(data1, out_unsorted, p)
#setorder(merged_data,-probability)
n0 <- ncol(data1)
n2 <- ncol(merged_data)
n1 <- n2-4

## calculate median of numeric predictors

fun1=function(x){
  ifelse(is.numeric(x),median(x,na.rm=T),NA)
}
para_median=sapply(merged_data[,5:n0,with=F],fun1)
para_median=c(rep(NA,4),para_median)

## for unoshow=TRUE

fun2=function(x){
  min(which(!is.na(x)))
}
model_n=apply(merged_data[,n1:n2,with=F],1,fun2)
n_para=sapply(para_list,length)

data=NULL
for(i in 1:5){
  data_i=merged_data[rep(which(model_n==i),each=n_para[i]),]
  if(nrow(data_i)>0){
    data_mi=data_i[,1:n0,with=F]
    sel_para <- para_list[[i]]
    para=rep(sel_para,nrow(data_mi)/n_para[i])
    for(j in 1:length(sel_para)){
      if(!is.na(para_median[sel_para[j]])){
        data_mi[para==sel_para[j],which(names(data_mi)==sel_para[j]):=para_median[sel_para[j]]]
      }
    }
    sel_model <- model_list[[i]]
    data_i[,p_median:=predict(sel_model, newdata=data_mi, type="response")]
    data_i[,p_abs_change:=probability-p_median]
    data_i[,p_rel_change:=(probability-p_median)/p_median]
    data_i[,sel_para:=para]
    data=rbind(data,data_i)
  }
  print(i)
}
output=data[,c(as.list(sel_para[order(abs(p_abs_change),decreasing=T)[1:5]]),
           as.list(p_abs_change[order(abs(p_abs_change),decreasing=T)[1:5]])),by=list(patient_id,facility_id,probability)]

setnames(output,c("patient_id", "facility_id", "probability", 
                               "top1_riskfactor", "top2_riskfactor","top3_riskfactor", "top4_riskfactor", "top5_riskfactor", 
                               "top1_p_abs_change", "top2_p_abs_change","top3_p_abs_change", "top4_p_abs_change", "top5_p_abs_change"))
out_final <- merge(x=out_unsorted, y=output, by=c("patient_id", "facility_id",'probability'))
setorder(out_final,-probability)

# output data
write.csv(out_final, "/home/rapp/R/pred016/rout_new.csv", na="", row.names=F)


