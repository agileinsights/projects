/*
create table DIAL_POP_DATA_FACT (
        source_id number,
        subject_id varchar(25),
        subject_id_source varchar(25),
        subject_json varchar(1000)
            CONSTRAINT ensure_json CHECK (subject_json IS JSON),
        insert_date date,
        insert_by number
)
*/



select json_value(DIAL_POP_DATA_FACT.subject_json, '$.call_made') from DIAL_POP_DATA_FACT