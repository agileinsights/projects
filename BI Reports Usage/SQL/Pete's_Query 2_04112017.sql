SELECT                                                     
         sl.user_id,
         pl.custom_clinicsite,
         TO_DATE(to_char(pl.start_time, 'MM/DD/YYYY HH24:MI'), 'MM/DD/YYYY HH24:MI') page_start,
         SUBSTR (PL.CS_URI_STEM, 2, (INSTR (PL.CS_URI_STEM, '/', 2) - 2)) BI_Application,
         pl.page_name,
         SUM(round((pl.e2e_time*.001),2)) E2E_Time_Sec,
         SUM(round((pl.NETWORK_TIME*.001),2)) NETWORK_TIME_Sec,
         SUM(round((pl.PROCESS_TIME*.001),2)) PROCESS_TIME_Sec,
         SUM(round((pl.ssl_time*.001),2)) ssl_time_Sec,
         SUM(round((pl.REDIRECT_SSL_TIME*.001),2)) REDIRECT_SSL_TIME_Sec
    FROM bi_log.page_log pl INNER JOIN bi_log.session_log sl ON pl.session_id = sl.session_id
   WHERE pl.cs_host = 'logixmlreports.intranet.fmcna.com'  --all Logi apps
           AND sl.user_id = '3172844'
           --and to_char(pl.start_time, 'MM/DD/YYYY') = '04/03/2017' --Remove... only use for testing purposes
GROUP BY   SUBSTR (PL.CS_URI_STEM, 2,
        (INSTR (PL.CS_URI_STEM, '/', 2) - 2)),
        sl.user_id,
        pl.custom_clinicsite,
        pl.page_name,
        TO_DATE(to_char(pl.start_time, 'MM/DD/YYYY HH24:MI'), 'MM/DD/YYYY HH24:MI')
ORDER BY 1,3