SELECT sl.user_id,
         pl.custom_clinicsite,
         TO_DATE(to_char(pl.start_time, 'MM/DD/YYYY HH24:MI'), 'MM/DD/YYYY HH24:MI') page_start,
         SUBSTR (PL.CS_URI_STEM, 2, (INSTR (PL.CS_URI_STEM, '/', 2) - 2)) BI_Application,
         pl.page_name,
         SUM(round((pl.e2e_time*.001),2)) E2E_Time_Sec,
         SUM(round((pl.NETWORK_TIME*.001),2)) NETWORK_TIME_Sec,
         SUM(round((pl.PROCESS_TIME*.001),2)) PROCESS_TIME_Sec,
         SUM(round((pl.ssl_time*.001),2)) ssl_time_Sec,
         SUM(round((pl.REDIRECT_SSL_TIME*.001),2)) REDIRECT_SSL_TIME_Sec
FROM page_log pl INNER JOIN session_log sl ON pl.session_id = sl.session_id
WHERE pl.cs_host = 'logixmlreports.intranet.fmcna.com' --all Logi apps
         AND sl.user_id = '3172844' and pl.start_time > '09-MAR-2017' --Remove... only use for testing purposes
GROUP BY   
SUBSTR (PL.CS_URI_STEM, 2, (INSTR (PL.CS_URI_STEM, '/', 2) - 2))
, sl.user_id
, TO_DATE(to_char(pl.start_time, 'MM/DD/YYYY HH24:MI'), 'MM/DD/YYYY HH24:MI')
, pl.custom_clinicsite
, pl.page_name

ORDER BY 1,2