rm(list = ls())

# Set seed for reproducible results 
# set.seed(100) 

# Packages 
library(tm) # Text mining: Corpus and Document Term Matrix 
library(class) # KNN model 
library(SnowballC) # Stemming words 

# Read csv with two columns: text and category 
setwd("~/Rajasekar Alamanda/SAO Model")
df0 <- read.csv("mapped descriptions 4 Raj 612.csv")
df0  <- subset(df0,df0$Category != "A")
df1 <- subset(df0,df0$Category == "A")

for (i in 1:10) {
  
# Create corpus
docs <- Corpus(VectorSource(df0$Text))

# Clean corpus 
docs <- tm_map(docs, content_transformer(tolower)) 
docs <- tm_map(docs, removeNumbers) 
docs <- tm_map(docs, removeWords, stopwords("english")) 
docs <- tm_map(docs, removePunctuation) 
docs <- tm_map(docs, stripWhitespace) 
docs <- tm_map(docs, stemDocument, language = "english") 


# Create dtm 
dtm <- DocumentTermMatrix(docs) 

# Transform dtm to matrix to data frame - df is easier to work with 
mat.df <- as.data.frame(data.matrix(dtm), stringsAsfactors = FALSE) 

# Column bind category (known classification) 
mat.df <- cbind(mat.df, df0$Category) #

# Change name of new column to "category" 
colnames(mat.df)[ncol(mat.df)] <- "category" 

# Split data by rownumber into two equal portions 
train <- sample(nrow(mat.df), ceiling(nrow(mat.df) * .70))
test  <- (1:nrow(mat.df))[- train]

 # train <- sample(nrow(df))
 # test <- sample(nrow(df1))

i1 <- df0$iteration
# 
# fullset <- cbind(1:nrow(df))
# randset <- fullset[sample(nrow(df))]
# train <- randset[sample(nrow(df))]
# fullset1 <- cbind(1:nrow(df1))
# randset1 <- fullset[sample(nrow(df1))]
# test <-  randset1[sample(nrow(df1))]

# Isolate classifier
cl <- mat.df[, "category"]
t1 <- df0$Text
n1 <- df0$count
i1 <- rep(i, nrow(df0))

# Create model data and remove "category" 
modeldata <- mat.df[,!colnames(mat.df) %in% "category"] 

# Create model: training set, test set, training set classifier 
knn.pred <- knn(modeldata[train, ], modeldata[test, ], cl[train]) 
 
# Confusion matrix 
conf.mat <- table("Predictions" = knn.pred, Actual = cl[test]) 
conf.mat 

# Accuracy 
(accuracy <- sum(diag(conf.mat))/length(test) * 100) 

# Create data frame with test data and predicted category 
df.pred <- cbind(knn.pred, modeldata[test, ]) 
# if (exists('out0') == TRUE) {
#   out0 <- rbind(out0, data.frame(t1[test], cl[test], n1[test], knn.pred, accuracy))
# }
# else {
  out0 <- data.frame(t1[test], cl[test], n1[test], knn.pred, accuracy)
# }
}
out1 <- data.frame(out0)
View(out1)
write.csv(out1, file = "out1.csv")

