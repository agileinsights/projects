-- need this by clinic and showing metric value
/*
select * 
from KCNG.ADMIN.Z_METRIC_TARGET_FACT
where modality = 'IHD'
order by metric_cd
*/

SELECT B.METRIC_DT, DL.DL_NM, B.METRIC_DIM_ID, 
D.METRIC_CD METRIC_CD, D.METRIC_DESC METRIC_DESC,
--B.METRIC_VALUE_TXT
--max(case when B.METRIC_VALUE_TXT is null then '0' else B.METRIC_VALUE_TXT end),
sum(to_number(case when B.METRIC_VALUE_TXT = '' then '0' else B.METRIC_VALUE_TXT end, '99999.99')),
SUM(CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END) CPA_PT_MET_TARGET_CNT,  
COUNT(DISTINCT B.PT_METRIC_FACT_ID) CPA_PT_TOT
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN KCNG.ADMIN.Z_PROGRAM_DIM C ON CPA.POPULATION_DIM_ID = C.POPULATION_DIM_ID
INNER JOIN KCNG.ADMIN.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
--WHERE B.METRIC_DT = '2015-03-01'
WHERE B.METRIC_DT in ('2015-03-01', '2015-02-01', '2015-01-01')
--AND B.METRIC_DIM_ID IN (159, 101, 229, 85, 29, 232, 32, 48, 131, 99, 235, 51, 43, 230, 30, 46, 129, 73, 1, 260, 132, 236, 228, 204, 130, 98, 34, 42, 10)
AND B.METRIC_DIM_ID IN (238, 228, 230, 43, 117, 62, 257, 240, 103, 27, 242, 104)
--AND (A.PT_DIM_ID > 0) AND (B.PT_DIM_ID > 0)
AND (B.DL_DIM_ID > 0)
--AND C.POPULATION_TYPE_CD = 2
AND C.PROGRAM_DIM_ID = 7 
AND D.MODALITY = 'IHD'
GROUP BY  B.METRIC_DT, DL.DL_NM, B.METRIC_DIM_ID, D.METRIC_CD, D.METRIC_DESC

/*
SELECT X.* FROM (
 
WITH DATA_ALL AS (
SELECT B.METRIC_DT, B.METRIC_DIM_ID, MAX(D.MODALITY) MODALITY,  MAX(D.METRIC_CD) METRIC_CD, COUNT(DISTINCT B.PT_METRIC_FACT_ID) ALL_PT_MET_TARGET_CNT, 
SUM(CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END) ALL_PT_TOT
FROM CS_QUALITY.ADMIN.DIALYSIS_PATIENT_POPULATION_FACT_H A
INNER JOIN CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B ON A.PT_DIM_ID = B.PT_DIM_ID
INNER JOIN KCNG.ADMIN.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
--WHERE B.METRIC_DT = '2015-03-01'
WHERE B.METRIC_DT in ('2015-03-01', '2015-02-01', '2015-01-01')
AND B.METRIC_DIM_ID IN (159, 101, 229, 85, 29, 232, 32, 48, 131, 99, 235, 51, 43, 230, 30, 46, 129, 73, 1, 260, 132, 236, 228, 204, 130, 98, 34, 42, 10)
AND (A.PT_DIM_ID > 0) AND (B.PT_DIM_ID > 0) AND (B.DL_DIM_ID > 0)
AND D.MODALITY = 'IHD'
GROUP BY B.METRIC_DT, B.METRIC_DIM_ID
) ,
 
DATA_OUT_PROG AS (
SELECT B.METRIC_DT, B.METRIC_DIM_ID, MAX(D.MODALITY) MODALITY,  MAX(D.METRIC_CD) METRIC_CD, COUNT(DISTINCT B.PT_METRIC_FACT_ID) OUT_PROG_PT_MET_TARGET_CNT, 
SUM(CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END) OUT_PROG_PT_TOT
FROM CS_QUALITY.ADMIN.DIALYSIS_PATIENT_POPULATION_FACT_H A
INNER JOIN CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B ON A.PT_DIM_ID = B.PT_DIM_ID
INNER JOIN KCNG.ADMIN.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
--WHERE B.METRIC_DT = '2015-03-01'
WHERE B.METRIC_DT in ('2015-03-01', '2015-02-01', '2015-01-01')
AND B.METRIC_DIM_ID IN (159, 101, 229, 85, 29, 232, 32, 48, 131, 99, 235, 51, 43, 230, 30, 46, 129, 73, 1, 260, 132, 236, 228, 204, 130, 98, 34, 42, 10)
AND (A.PT_DIM_ID > 0)
AND (B.PT_DIM_ID > 0)
AND (B.DL_DIM_ID > 0)
AND NOT EXISTS (SELECT 'X' FROM CS_QUALITY.ADMIN.DIALYSIS_PATIENT_POPULATION_FACT_H Y INNER JOIN KCNG.ADMIN.Z_PROGRAM_DIM Z
ON Y.POPULATION_DIM_ID = Z.POPULATION_DIM_ID AND Z.PROGRAM_DIM_ID = 6
WHERE A.PT_DIM_ID = Y.PT_DIM_ID)
AND D.MODALITY = 'IHD'
GROUP BY B.METRIC_DT, B.METRIC_DIM_ID
) ,
 
 
 
DATA_IN_CPA AS (
SELECT B.METRIC_DT, C.PROGRAM_DIM_ID,  C.INITIATIVE_DIM_ID, CPA.POPULATION_DIM_ID, B.METRIC_DIM_ID, MAX(D.MODALITY) MODALITY,
MAX(C.PROGRAM_CD) PROGRAM_CD, MAX(C.INITIATIVE_CD) INITIATIVE_CD, MAX(C.POPULATION_CD) POPULATION_CD, MAX(D.METRIC_CD) METRIC_CD, MAX(D.METRIC_DESC) METRIC_DESC,
COUNT(DISTINCT B.PT_METRIC_FACT_ID) CPA_PT_MET_TARGET_CNT,  SUM(CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END) CPA_PT_TOT
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN KCNG.ADMIN.Z_PROGRAM_DIM C ON CPA.POPULATION_DIM_ID = C.POPULATION_DIM_ID
INNER JOIN KCNG.ADMIN.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
--WHERE B.METRIC_DT = '2015-03-01'
WHERE B.METRIC_DT in ('2015-03-01', '2015-02-01', '2015-01-01')
AND B.METRIC_DIM_ID IN (159, 101, 229, 85, 29, 232, 32, 48, 131, 99, 235, 51, 43, 230, 30, 46, 129, 73, 1, 260, 132, 236, 228, 204, 130, 98, 34, 42, 10)
--AND (A.PT_DIM_ID > 0) AND (B.PT_DIM_ID > 0)
AND (B.DL_DIM_ID > 0)
--AND C.POPULATION_TYPE_CD = 2
AND C.PROGRAM_DIM_ID = 7 
AND D.MODALITY = 'IHD'
GROUP BY  B.METRIC_DT, C.PROGRAM_DIM_ID,  C.INITIATIVE_DIM_ID, CPA.POPULATION_DIM_ID, B.METRIC_DIM_ID
)
 
 
SELECT A.METRIC_DT, A.PROGRAM_DIM_ID, A.PROGRAM_CD,  A.INITIATIVE_DIM_ID, A.INITIATIVE_CD, A.POPULATION_DIM_ID, A.POPULATION_CD, A.METRIC_DIM_ID, A.MODALITY,
A.METRIC_CD, A.METRIC_DESC, A.CPA_PT_MET_TARGET_CNT,  A.CPA_PT_TOT, B.OUT_PROG_PT_MET_TARGET_CNT,  B.OUT_PROG_PT_TOT, C.ALL_PT_MET_TARGET_CNT,  C.ALL_PT_TOT
FROM DATA_IN_CPA A
INNER JOIN DATA_OUT_PROG B ON A.METRIC_DT = B.METRIC_DT AND A.METRIC_DIM_ID = B.METRIC_DIM_ID
INNER JOIN DATA_ALL C ON A.METRIC_DT = C.METRIC_DT AND A.METRIC_DIM_ID = C.METRIC_DIM_ID
) X
 
WHERE EXISTS (SELECT 'X' FROM KCNG.ADMIN.Z_METRIC_TARGET_FACT Z WHERE X.METRIC_DIM_ID = Z.METRIC_DIM_ID AND '2015-01-15' BETWEEN Z.METRIC_TARGET_START_DT AND Z.METRIC_TARGET_END_DT AND Z.IS_METRIC_PRIMARY_IND IS TRUE)
ORDER BY X.METRIC_DT, X.PROGRAM_DIM_ID, X.MODALITY ASC

*/