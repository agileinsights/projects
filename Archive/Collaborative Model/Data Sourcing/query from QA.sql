SELECT DL_NM, METRIC_DESC,METRIC_DT, SUM(NUMERATOR)as NUMERATOR,SUM(DENOMINATOR)as DENOMINATOR FROM((select 'DB_WO_FC' as metric_cd,1 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'imofootcheck' as metricabbrv, '1 Month Foot Check' as metriclabel
union select 'FIST_WO_CATH' as metric_cd,2 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'fistula' as metricabbrv, 'A-V Fistula w/o catheter' as metriclabel
union select 'ALBUMIN_3' as metric_cd,3 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'albumin' as metricabbrv, 'Albumin >= 4.0 g / dL' as metriclabel
union select 'V17_CATHETERS_3' as metric_cd,4 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'catheters' as metricabbrv, 'Catheters (none>90 days)' as metriclabel
union select 'V17_CMB_3' as metric_cd,5 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'compbonemin' as metricabbrv, 'CBM (Ca<=10.0, P 3.0-5.5, IPTH 150-600)' as metriclabel
union select 'HEMOGLOBIN_3' as metric_cd,6 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'hemoglobin' as metricabbrv, 'Hemoglobin 11-12 g/dl' as metriclabel
union select 'SPKTV_OR_WSTDKTV_3' as metric_cd,7 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'skkt' as metricabbrv, 'spKt / V >= 1.2' as metriclabel
union select 'TRNS_EDU' as metric_cd,8 as sortorder,'clinical' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'transplanted' as metricabbrv, 'Transplant Education' as metriclabel
union select 'EBIT' as metric_cd,9 as sortorder,'financial' as metricgroup,0 as decimal,1 as scale,'$' as symbol,'' as suffix,'good' as incrMean,'ebitpertmt' as metricabbrv, 'EBIT $/tmt' as metriclabel
union select 'EBITPCT' as metric_cd,10 as sortorder,'financial' as metricgroup,1 as decimal,100 as scale,'' as symbol,'%' as suffix,'good' as incrMean,'ebitpct' as metricabbrv, 'Ebit %' as metriclabel
union select 'tot_med_supply' as metric_cd,11 as sortorder,'financial' as metricgroup,0 as decimal,-1 as scale,'$' as symbol,'' as suffix,'bad' as incrMean,'medsuppliespertmt' as metricabbrv, 'Med Supplies $/tmt' as metriclabel
union select 'net_revenue' as metric_cd,12 as sortorder,'financial' as metricgroup,0 as decimal,1 as scale,'$' as symbol,'' as suffix,'good' as incrMean,'netrevpertmt' as metricabbrv, 'Net Revenue $/tmt' as metriclabel
union select 'Operating_Expenses' as metric_cd,13 as sortorder,'financial' as metricgroup,0 as decimal,-1 as scale,'$' as symbol,'' as suffix,'bad' as incrMean,'totopcostpertmt' as metricabbrv, 'Total Operating Cost $/tmt' as metriclabel
union select 'Total_Salaries' as metric_cd,14 as sortorder,'financial' as metricgroup,0 as decimal,-1 as scale,'$' as symbol,'' as suffix,'bad' as incrMean,'totalsalarypertmt' as metricabbrv, 'Total Salaries $/tmt' as metriclabel
union select 'PSI' as metric_cd,15 as sortorder,'financial' as metricgroup,2 as decimal,1 as scale,'' as symbol,'' as suffix,'bad' as incrMean,'avgpsi' as metricabbrv, 'Avg Pers Score Index' as metriclabel
union select 'qual_outcome' as metric_cd,16 as sortorder,'financial' as metricgroup,0 as decimal,1 as scale,'' as symbol,'' as suffix,'good' as incrMean,'qualityoutcomes' as metricabbrv, 'Quality Outcomes' as metriclabel
union select 'TOTHOSDAYS_1' as metric_cd,17 as sortorder,'operational' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'bad' as incrMean,'hospitalrate' as metricabbrv, 'Hospitalization Rate' as metriclabel
union select 'SMR' as metric_cd,18 as sortorder,'operational' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'bad' as incrMean,'mortalityrate' as metricabbrv, 'Mortality Rate' as metriclabel
union select 'READMIT' as metric_cd,19 as sortorder,'operational' as metricgroup,0 as decimal,1 as scale,'' as symbol,'%' as suffix,'bad' as incrMean,'readmissrate' as metricabbrv, 'Readmission Rate' as metriclabel
union select 'P2_GT1_UNEX_MISSED_TX_1' as metric_cd,20 as sortorder,'operational' as metricgroup,1 as decimal,1 as scale,'' as symbol,'%' as suffix,'bad' as incrMean,'missedtmtpct' as metricabbrv, 'Total % Missed Treatments (1 mo)' as metriclabel
union select 'ic_tmt' as metric_cd,21 as sortorder,'operational' as metricgroup,0 as decimal,1 as scale,'' as symbol,'' as suffix,'good' as incrMean,'totalictmt' as metricabbrv, 'Total In Center Tmt' as metriclabel
union select 'US_SUB' as metric_cd,22 as sortorder,'operational' as metricgroup,1 as decimal,1 as scale,'' as symbol,'' as suffix,'good' as incrMean,'ultrascore' as metricabbrv, 'UltraScore Subtotal' as metriclabel) AS J
inner join
(SELECT B.METRIC_DT, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,
-- SUM(CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END) numerator,
--COUNT(DISTINCT B.PT_METRIC_FACT_ID) denominator
COUNT (
DISTINCT (CASE
WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
THEN B.PT_DIM_ID
ELSE NULL
END))
AS numerator,
COUNT(DISTINCT B.PT_DIM_ID) denominator
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'
WHERE B.METRIC_DT = '2016-01-01'
-- AND B.METRIC_DIM_ID IN (73 , 131, 238, 228, 230, 43, 117, 62, 257, 240, 103, 27, 242, 104, 72, 71, 259, 15, 70, 200, 57, 85, 44, 67)
AND (B.DL_DIM_ID > 0)
AND P.PROGRAM_DIM_ID = 7
-- AND M.METRIC_NM = 'IHD'
AND B.METRIC_DIM_ID IN (
73, --spkt/v
1, -- hemo
131, -- CBM
228, -- albumin
85, -- catheters
238, -- footcheck (needs to be fixed in oracle)
104, -- fistula without catheter
27, -- transplant education
44, -- hospitalization rate
-- readmission rate
72 -- missed treatments
-- standard mortality rate (not in KCNG)
)
GROUP BY B.METRIC_DT, CPA.DL_POPULATION_START_DT, DL.DL_NM, DL.DL_ID, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC
-- ultrascore subtotal
union
select METRIC_DT, DL_POPULATION_START_DT,
DL_NM, DL_ID, 888, 'US_SUB' METRIC_CD, 'Ultrascore Subtotal' METRIC_DESC,
sum(round(cast(numerator as float)/cast(denominator as float), 3)*100) as numerator, 1 as denominator
from (
SELECT B.METRIC_DT, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,
COUNT (
DISTINCT (CASE
WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
THEN B.PT_DIM_ID
ELSE NULL
END))
AS numerator,
COUNT(DISTINCT B.PT_DIM_ID) denominator
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'
WHERE B.METRIC_DT = '2016-01-01'
AND (B.DL_DIM_ID > 0)
AND P.PROGRAM_DIM_ID = 7
AND B.METRIC_DIM_ID IN (
73, --spkt/v
1, -- hemo
131, -- CBM
228, -- albumin
85 -- catheters
)
GROUP BY B.METRIC_DT, CPA.DL_POPULATION_START_DT, DL.DL_NM, DL.DL_ID, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC
) primaryindicators
group by METRIC_DT, DL_POPULATION_START_DT, DL_NM, DL_ID
-- expenses, salaries and ebit
union
SELECT
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
999 as METRIC_DIM_ID, translate(FN_CLASS_NAME, ' ', '_') as METRIC_CD, FN_CLASS_NAME as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
WHERE P.PROGRAM_DIM_ID = 7
and FN_CLASS_NAME in ('EBIT', 'Operating Expenses', 'Total Salaries', 'Total Medical Supplies Expenses')
and PL.post_Date_id = '20160101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID, FN_CLASS_NAME
-- medical expenses
union
SELECT
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
999 as METRIC_DIM_ID, 'tot_med_supply' as METRIC_CD, 'Total Medical Supplies Expenses' as METRIC_DESC,
sum(POST_AMT) AS numerator, sum(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
WHERE P.PROGRAM_DIM_ID = 7
and FN_ACCT_NAME = 'Total Medical Supplies Expenses'
and PL.post_Date_id = '20160101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID
-- net revenue
union
SELECT
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
999 as METRIC_DIM_ID, 'net_revenue' as METRIC_CD, 'Net Revenue' as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
WHERE P.PROGRAM_DIM_ID = 7
and FN_CLASS_NAME in ('Commercial Revenue','Medicaid Risk Revenue','Medicare Revenue','Medicare Advantage Revenue','Medicaid Revenue','Old Revenue',
'Misc Revenue','Self Pay Revenue','Acute Revenue','Vascular Revenue','Bad Debt Revenue','Pre Acute Revenue','CKD Class Revenue','Other Class Revenue','Interest & Taxes')
and PL.post_Date_id = '20160101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID
-- in center treatments
union
SELECT
TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID,
999 as METRIC_DIM_ID, 'ic_tmt' as METRIC_CD, 'In Center Treatments' as METRIC_DESC,
sum(GL.ACTL_IC_TMT_CNT) As numerator, 1 AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = GL.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON DL.DL_DIM_ID = CPA.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
WHERE P.PROGRAM_DIM_ID = 7
and GL.post_Date_id = '20160101'
group by TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID
　
union
-- All FMC
-- quality metrics
SELECT B.METRIC_DT, TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,
COUNT (
DISTINCT (CASE
WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
THEN B.PT_DIM_ID
ELSE NULL
END))
AS numerator,
COUNT(DISTINCT B.PT_DIM_ID) denominator
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'
WHERE B.METRIC_DT = '2016-01-01'
AND (B.DL_DIM_ID > 0)
AND B.METRIC_DIM_ID IN (
73, --spkt/v
1, -- hemo
131, -- CBM
228, -- albumin
85, -- catheters
238, -- footcheck (needs to be fixed in oracle)
104, -- fistula without catheter
27, -- transplant education
44, -- hospitalization rate
-- readmission rate
72 -- missed treatments
-- standard mortality rate (not in KCNG)
)
GROUP BY B.METRIC_DT, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC
-- ultrascore subtotal
union
select METRIC_DT, DL_POPULATION_START_DT,
DL_NM, DL_ID, 888, 'US_SUB' METRIC_CD, 'Ultrascore Subtotal' METRIC_DESC,
sum(round(cast(numerator as float)/cast(denominator as float), 3)*100) as numerator, 1 as denominator
from (
SELECT B.METRIC_DT, TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,
COUNT (
DISTINCT (CASE
WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
THEN B.PT_DIM_ID
ELSE NULL
END))
AS numerator,
COUNT(DISTINCT B.PT_DIM_ID) denominator
FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'
WHERE B.METRIC_DT = '2016-01-01'
AND (B.DL_DIM_ID > 0)
AND B.METRIC_DIM_ID IN (
73, --spkt/v
1, -- hemo
131, -- CBM
228, -- albumin
85 -- catheters
)
GROUP BY B.METRIC_DT, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC
) primaryindicators
group by METRIC_DT, DL_POPULATION_START_DT, DL_NM, DL_ID
　
-- expenses, salaries and ebit
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
SELECT PL.DL_DIM_ID,
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,
TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
999 as METRIC_DIM_ID, translate(FN_CLASS_NAME, ' ', '_') as METRIC_CD, FN_CLASS_NAME as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID
WHERE FN_CLASS_NAME in ('EBIT', 'Operating Expenses', 'Total Salaries', 'Total Medical Supplies Expenses')
and PL.post_Date_id = '20160101'
group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1,
FN_CLASS_NAME
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC
-- medical expenses
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
SELECT PL.DL_DIM_ID,
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,
TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
999 as METRIC_DIM_ID, 'tot_med_supply' as METRIC_CD, 'Total Medical Supplies Expenses' as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID
WHERE FN_ACCT_NAME = 'Total Medical Supplies Expenses'
and PL.post_Date_id = '20160101'
group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC
-- net revenue
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
SELECT PL.DL_DIM_ID,
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,
TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
999 as METRIC_DIM_ID, 'net_revenue' as METRIC_CD, 'Net Revenue' as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID
WHERE FN_CLASS_NAME in ('Commercial Revenue','Medicaid Risk Revenue','Medicare Revenue','Medicare Advantage Revenue','Medicaid Revenue','Old Revenue',
'Misc Revenue','Self Pay Revenue','Acute Revenue','Vascular Revenue','Bad Debt Revenue','Pre Acute Revenue','CKD Class Revenue','Other Class Revenue','Interest & Taxes')
and PL.post_Date_id = '20160101'
group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC
-- in center treatments
union
SELECT
TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,
TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID,
999 as METRIC_DIM_ID, 'ic_tmt' as METRIC_CD, 'In Center Treatments' as METRIC_DESC,
sum(GL.ACTL_IC_TMT_CNT) As numerator, 1 AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = GL.DL_DIM_ID
WHERE GL.post_Date_id = '20160101'
group by TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1) as G
on G.METRIC_CD = J.METRIC_CD
inner join
(select 'BMA EUREKA' as Facility, 'NORTHERN CALIFORNIA REGION' as Region,'Collaborative Model' as Program,1137 as facility_id, '1/1/2099' as enroll_date
union select 'BREMEN DIALYSIS CENTER' as Facility, 'NORTH GEORGIA' as Region,'Collaborative Model' as Program,6044 as facility_id, '1/1/2099' as enroll_date
union select 'CARROLL COUNTY DIALYSIS CENTER' as Facility, 'NORTH GEORGIA' as Region,'Collaborative Model' as Program,6045 as facility_id, '1/1/2099' as enroll_date
union select 'CENTRAL NEW MEXICO KIDNEY CENTER' as Facility, 'AUSTIN - NEW MEXICO' as Region,'Collaborative Model' as Program,2762 as facility_id, '1/1/2099' as enroll_date
union select 'COASTAL DIALYSIS CENTER' as Facility, 'SOUTH GEORGIA' as Region,'Collaborative Model' as Program,1561 as facility_id, '1/1/2099' as enroll_date
union select 'DIALYSIS OF GOLDEN ISLES' as Facility, 'SOUTH GEORGIA' as Region,'Collaborative Model' as Program,6921 as facility_id, '1/1/2099' as enroll_date
union select 'MARYVALE JV' as Facility, 'ARIZONA' as Region,'Collaborative Model' as Program,6279 as facility_id, '8/1/2015' as enroll_date
union select 'MCKINLEYVILLE' as Facility, 'WEST REGION' as Region,'Collaborative Model' as Program,3041 as facility_id, '1/1/2099' as enroll_date
union select 'MUSEUM DISTRICT 2' as Facility, 'HOUSTON REGION' as Region,'Collaborative Model' as Program,3869 as facility_id, '1/1/2099' as enroll_date
union select 'NORTH HOUSTON' as Facility, 'HOUSTON REGION' as Region,'Collaborative Model' as Program,1310 as facility_id, '6/1/2015' as enroll_date
union select 'OCHSNER NEW ORLEANS' as Facility, 'MISS/SELA' as Region,'Collaborative Model' as Program,4722 as facility_id, '8/1/2015' as enroll_date
union select 'PASADENA' as Facility, 'HOUSTON REGION' as Region,'Collaborative Model' as Program,1185 as facility_id, '6/1/2015' as enroll_date
union select 'PHILADELPHIA' as Facility, 'SOUTHERN NEW JERSEY' as Region,'Collaborative Model' as Program,1120 as facility_id, '1/1/2099' as enroll_date
union select 'UNIV OF ROCHESTER - STRONG MEM''L HEMO PROG.' as Facility, 'WESTERN NY' as Region,'Collaborative Model' as Program,2916 as facility_id, '8/1/2015' as enroll_date
union select 'All FMC' as Facility, 'All FMC' as Region,'Collaborative Model' as Program,999999 as facility_id, '1/1/2015' as enroll_date) as H
on H.FACILITY_ID = G.DL_ID) as V
WHERE METRIC_DESC = 'Ultrascore Subtotal'
GROUP BY DL_NM, METRIC_DESC, METRIC_DT;