create or replace view Z_NSM_OUTPUT_FACT as
(
SELECT 
source_nm,
DIAL_POP_DATA_FACT.subject_id as MRN,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_ID') as ESCO_ID,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.ESCO_MARKET') as ESCO_MARKET,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPCLINICNUM') as FHPCLINICNUM
FROM "BI_SRC"."DIAL_POP_DATA_FACT" "DIAL_POP_DATA_FACT"
where DIAL_POP_DATA_FACT.source_id in (5)