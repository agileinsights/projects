SELECT dl.dl_nm  Facility_name,
       dl.dl_id  Facility_id,
       a.POST_DATE_ID,
       SUM (a.ACTL_IC_TMT_CNT) AS Ic_tmt_cnt,   
       SUM (a.ACTL_DIAL_MOD_TMT_CNT) AS Current_yr_org_total_Treatments , 
       SUM (a.BASE_DIAL_MOD_TMT_CNT) AS Prior_yr_org_total_Treatments,
       Case when sum(a.BASE_DIAL_MOD_TMT_CNT) > 0 then
                   (sum(a.ACTL_DIAL_MOD_TMT_CNT)/sum(a.BASE_DIAL_MOD_TMT_CNT))*100
                   else 0 end  as org_treatment_growth_percent,
       sum(a.COMMERCIAL_TMT_CNT ) COMMERCIAL_TMT_CNT
  FROM    CS_REVENUE_NEW..PC_GL_PT_TX_AGGREGATE_FACT_H a,
         CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL
        where a.dl_dim_id = dl.dl_dim_id
        and a.post_date_id > '20150101' and a.post_date_id < '20160101'
   group by a.DL_DIM_ID , a.POST_DATE_ID, dl.dl_nm, dl.dl_id
   order by a.dl_dim_id