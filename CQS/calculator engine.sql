--------------------------------------------------------
--- crate pipeline function to build dynamic queries ---
--------------------------------------------------------

-- define an object type

drop TYPE metricDataObject_ntt;

create or replace TYPE metricDataObject AS OBJECT (

   	post_date DATE,
   	hierarchy_label VARCHAR2(225),
   	hierarchy_id VARCHAR2(25),
   	hierarchy_level VARCHAR2(25),
   	metric_total VARCHAR2(225),
   	metric_category VARCHAR2(225),
   	metric_group VARCHAR2(225),
   	metric_label VARCHAR2(225),
	metric_id number(18,6),
   	metric_modality VARCHAR2(225),
	metric_goal_low number(18,6),
	metric_goal_med number(18,6),
	metric_goal_high number(18,6),
	metric_scale number(18,6),
	metric_up_meaning number(18,6),
	metric_numerator number(18,6),
	metric_denominator number(18,6),
	metric_value number(18,6),
	ultrascore number(18,6),
	standardized_score number(18,6),
	possible_score number(18,6),
	cqs_score number(18,6),
	cqs_flag number(18,6),
	us_flag number(18,6)

   );

-- define type "table" of MyObjects
create TYPE metricDataObject_ntt IS TABLE OF metricDataObject ;

--/
    create or replace FUNCTION get_metricDataObjects(
        p_filter_global_id VARCHAR2,
        p_filter_hierarchy_level INT,
        p_hierarchy_name VARCHAR2,
        p_start_date VARCHAR2,
        p_end_date VARCHAR2,
        p_grouping_hierarchy_level INT,
        p_modality VARCHAR2,
        p_grouping_metric_level INT,
        p_scenario VARCHAR2
    
    ) RETURN metricDataObject_ntt 
    PIPELINED
    PARALLEL_ENABLE
    IS
        cur sys_refcursor ;

   	post_date DATE ;
   	hierarchy_label VARCHAR2(225) ;
   	hierarchy_id VARCHAR2(25) ;
   	hierarchy_level VARCHAR2(25) ;
   	metric_total VARCHAR2(225) ;
   	metric_category VARCHAR2(225) ;
   	metric_group VARCHAR2(225);
   	metric_label VARCHAR2(225) ;
	metric_id number(18,6);
   	metric_modality VARCHAR2(225);
	metric_goal_low number(18,6); 
	metric_goal_med number(18,6); 
	metric_goal_high number(18,6); 
	metric_scale number(18,6) ;
	metric_up_meaning number(18,6) ;
	metric_numerator number(18,6) ;
	metric_denominator number(18,6) ;
	metric_value number(18,6) ;
	ultrascore number(18,6) ;
	standardized_score number(18,6) ;
	possible_score number(18,6) ;
	cqs_score number(18,6) ;
	cqs_flag number(18,6) ;
	us_flag number(18,6) ;
	
        sqlrequest varchar2(20000) := '
        
        -- Detailed view --
        with metric_detail as (
        select 
                f.name as hierarchy_label,
                f.id as hierarchy_id,
                f."level" as hierarchy_level,
                m.post_date, 
                m.METRIC_ID,
                m.metric_rank, 
                m.fac_id,
                md.METRIC_CD,
                md.metric_calc, 
                ' || p_grouping_metric_level || ' as metric_level,
                md.METRIC_CATEGORY,
                md.metric_group, 
                md.METRIC_LABEL,
                md.metric_modality, 
                nvl(sc.cqs_flag, md.cqs_flag) cqs_flag, 
                md.us_flag, 
                md.us_deduction,
                case when md.METRIC_UP_MEANING = ''good'' then 1 else -1 end as METRIC_UP_MEANING,
                md.METRIC_SCALE,
                nvl(round(sc.METRIC_GOAL_LOW_S, 5), round(md.METRIC_GOAL_LOW, 5)) METRIC_GOAL_LOW,
                nvl(round(sc.METRIC_GOAL_MED_S, 5), round(md.METRIC_GOAL_MED, 5)) METRIC_GOAL_MED,
                nvl(round(sc.METRIC_GOAL_HIGH_S, 5), round(md.METRIC_GOAL_HIGH, 5)) METRIC_GOAL_HIGH,
                nvl(round(sc.METRIC_WEIGHT_S, 5), 10) METRIC_WEIGHT,
        
        sum(case when m.metric_position = ''numerator'' then nvl(metric_value,0) else 0 end) as numerator,
        sum(case when m.metric_position = ''denominator'' then nvl(metric_value,0) else 0 end) as denominator
        from 
        (
                select to_number(subject_id) as fac_id, f.bit_id, post_date, ml.metric_id, ml.metric_calc, ml.metric_modality, ml.metric_group, metric_position, metric_rank, metric_value, ml.cqs_flag, ml.us_flag, ml.us_deduction
                from metric_data_fact m inner join (
                        select metric_id, mr.metric_calc, mr.metric_modality, mr.metric_group, METRIC_part, metric_position, DENSE_RANK() OVER (ORDER BY metric_id) metric_rank, cqs_flag, us_flag, us_deduction
                        from (				
                                select metric_id, metric_calc, metric_modality, metric_group, metric_numerator as METRIC_part, ''numerator'' as metric_position, cqs_flag, us_flag, us_deduction
                                FROM METRIC_DATA_DIM 
                                where (cqs_flag = 1 or us_flag = 1)
                                and metric_calc = ''Rate''
                                and metric_modality in (' || p_modality || ')
                                union 
                                select metric_id, metric_calc, metric_modality, metric_group, metric_denominator as METRIC_part, ''denominator'' as metric_position, cqs_flag, us_flag, us_deduction
                                FROM METRIC_DATA_DIM 
                                where (cqs_flag = 1 or us_flag = 1)
                                and metric_calc = ''Rate''
                                and metric_denominator <> ''1''
                                and metric_modality in (' || p_modality || ')
                        ) mr
                ) ml on m.METRIC_CD = ml.metric_part
                and post_date >= trunc(to_date(''' || p_start_date || ''', ''YYYY-MM-DD''), ''MONTH'')
                and post_date < trunc(to_date(''' || p_end_date || ''', ''YYYY-MM-DD'') + 1, ''MONTH'')
                
                inner join FAC_DIAL_HIERARCHY f 
                on m.subject_id = f.global_id
                and f."level" = ''facility''

                and hierlevel(f.bit_id, ' || p_filter_hierarchy_level || ', 6) in (' || p_filter_global_id || ')
                and hierarchy = ''' || p_hierarchy_name || '''
                
        ) m inner join metric_data_dim md on 
                m.metric_id = md.metric_id
            left join (
                        SELECT st.*
                        from (
                                select
                                case when ''' || p_scenario || ''' = ''''
                                    then ''{"rows": [{"id": "0","name": "0","current": [0,0,0,0],"new": [0,0,0,0]}]}''
                                    else ''' || p_scenario || ''' end as scenario
                                from dual
                        
                        ) sdata,
                        JSON_TABLE(scenario, ''$.rows[*]''
                        COLUMNS (
                                METRIC_ID VARCHAR2(20) PATH ''$.id'',
                                CQS_FLAG VARCHAR2(20) PATH ''$.cqs_flag'',
                                METRIC_GOAL_LOW_S VARCHAR2(20) PATH ''$.new[0]'',
                                METRIC_GOAL_MED_S VARCHAR2(20) PATH ''$.new[1]'',
                                METRIC_GOAL_HIGH_S VARCHAR2(20) PATH ''$.new[2]'',
                                METRIC_WEIGHT_S VARCHAR2(20) PATH ''$.new[3]''
                        )) AS st
                        ) sc
             on md.metric_id = sc.metric_id
                
          inner join FAC_DIAL_HIERARCHY f 
                on hierlevel(m.bit_id, ' || p_grouping_hierarchy_level || ', 6) = to_number(f.global_id)
                and f.level_position = ' || p_grouping_hierarchy_level || '
                and hierarchy = ''' || p_hierarchy_name || '''

        group by         
                f.name,
                f.id,
                f."level",
                m.post_date, 
                m.METRIC_ID,
                m.metric_rank, 
                m.fac_id,
                md.METRIC_CD,
                md.metric_calc, 
                md.METRIC_CATEGORY,
                md.metric_group,
                md.METRIC_LABEL,
                md.metric_modality,
                nvl(sc.cqs_flag, md.cqs_flag), 
                md.us_flag, 
                md.us_deduction,
                case when md.METRIC_UP_MEANING = ''good'' then 1 else -1 end,
                md.METRIC_SCALE,
                nvl(round(sc.METRIC_GOAL_LOW_S, 5), round(md.METRIC_GOAL_LOW, 5)),
                nvl(round(sc.METRIC_GOAL_MED_S, 5), round(md.METRIC_GOAL_MED, 5)),
                nvl(round(sc.METRIC_GOAL_HIGH_S, 5), round(md.METRIC_GOAL_HIGH, 5)),
                nvl(round(sc.METRIC_WEIGHT_S, 5), 10)
        )
        
        -- Summarized calculations --
     select 
        trunc(post_date, ''MONTH'') post_date,
        hierarchy_label,
        hierarchy_id,
        hierarchy_level,
        ''Total'' as metric_total,
        case when metric_level = 1 then ''Total'' 
                else metric_category
                end as metric_category, 
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                else metric_group 
                end as metric_group,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_label 
                end as metric_label,
        case when metric_level = 1 then 0 
                when metric_level = 2 then 0
                when metric_level = 3 then 0
                else metric_id 
                end as metric_id,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_modality 
                end as metric_modality,

        case when metric_level < 4 then null else METRIC_GOAL_LOW end as METRIC_GOAL_LOW,
        case when metric_level < 4 then null else METRIC_GOAL_MED end as METRIC_GOAL_MED,
        case when metric_level < 4 then null else METRIC_GOAL_HIGH end as METRIC_GOAL_HIGH,
        case when metric_level < 3 then null else METRIC_SCALE end as METRIC_SCALE,
        case when metric_level < 3 then null else METRIC_UP_MEANING end as METRIC_UP_MEANING,
        case when metric_level < 3 then sum(0) else sum(metric_numerator) end metric_numerator, 
        case when metric_level < 3 then sum(0) else sum(metric_denominator) end metric_denominator, 
        case when metric_level < 3 then sum(0) else 
                decode(sum(metric_denominator), 0, 0, sum(metric_numerator)/sum(metric_denominator)) end metric_value,        
        case 
                when sum(case when us_deduction > 0 then 1 else 0 end) < 5 and metric_level < 3 then 0 
                else sum(metric_value * us_deduction * us_flag * 100) end ultrascore,
        sum(standardized_score * cqs_flag) as standardized_score,
        sum(possible_score) as possible_score,
        decode(sum(possible_score), 0, null, sum(standardized_score * cqs_flag) / sum(possible_score) * 100) as cqs_score,
        max(cqs_flag) as cqs_flag, 
        max(us_flag) as us_flag
        from (

        select 
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
          	metric_level,
                metric_calc,
                metric_id,
                metric_category, 
                metric_group,
                metric_label,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_WEIGHT,
                METRIC_SCALE,
                cqs_flag, 
                us_flag, 
                us_deduction,
                METRIC_UP_MEANING,
                sum(numerator) metric_numerator, 
                sum(denominator) metric_denominator, 
                decode(sum(denominator), 0, null, sum(numerator)/sum(denominator)) metric_value,
                decode(sum(denominator), 0, null,
                case 
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_LOW then 
                                0
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_MED then 
                                floor(0.4*METRIC_WEIGHT * ((METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) - METRIC_UP_MEANING * METRIC_GOAL_LOW) / 
                                        (METRIC_UP_MEANING * METRIC_GOAL_MED - METRIC_UP_MEANING * METRIC_GOAL_LOW)) + 0.1*METRIC_WEIGHT)
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_HIGH then 
                                floor(0.5*METRIC_WEIGHT * ((METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) - METRIC_UP_MEANING * METRIC_GOAL_MED) / 
                                        (METRIC_UP_MEANING * METRIC_GOAL_HIGH - METRIC_UP_MEANING * METRIC_GOAL_MED)) + 0.5*METRIC_WEIGHT)
                        else METRIC_WEIGHT end) as standardized_score,
                METRIC_WEIGHT as possible_score
                
        from METRIC_DETAIL
        group by
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
          	metric_level,
                metric_calc,
                metric_id,
                metric_category, 
                metric_group,
                metric_label,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_WEIGHT,
                METRIC_SCALE,
                cqs_flag, 
                us_flag, 
                us_deduction,
                METRIC_UP_MEANING
                ) mg
     group by
        trunc(post_date, ''MONTH''),
        hierarchy_label,
        hierarchy_id,
        hierarchy_level,
        case when metric_level = 1 then ''Total'' 
                else metric_category
                end, 
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                else metric_group 
                end,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_label 
                end,
        case when metric_level = 1 then 0 
                when metric_level = 2 then 0
                when metric_level = 3 then 0
                else metric_id 
                end,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_modality 
                end,

        case when metric_level < 4 then null else METRIC_GOAL_LOW end,
        case when metric_level < 4 then null else METRIC_GOAL_MED end,
        case when metric_level < 4 then null else METRIC_GOAL_HIGH end,
        case when metric_level < 3 then null else METRIC_SCALE end,
        case when metric_level < 3 then null else METRIC_UP_MEANING end,
        metric_level
         ';
        
        BEGIN
        
        OPEN cur FOR sqlrequest;
        LOOP
            FETCH cur INTO
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
                metric_total,
                metric_category, 
                metric_group,
                metric_label,
                metric_id,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                METRIC_UP_MEANING,
                metric_numerator, 
                metric_denominator, 
                metric_value,
                ultrascore,
                standardized_score,
                possible_score,
                cqs_score,
                cqs_flag,
                us_flag
             ;
            EXIT WHEN cur% notfound ;
            PIPE ROW(NEW metricDataObject(
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
                metric_total,
                metric_category, 
                metric_group,
                metric_label,
                metric_id,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                METRIC_UP_MEANING,
                metric_numerator, 
                metric_denominator, 
                metric_value,
                ultrascore,
                standardized_score,
                possible_score,
                cqs_score,
                cqs_flag,
                us_flag
            ));
        END LOOP ;
        RETURN;
    END ;
/


// view for metrics with numerator and denominator


DROP MATERIALIZED VIEW BI_SRC.METRIC_DATA_FACT_MV;

CREATE MATERIALIZED VIEW METRIC_DATA_FACT_MV
--PCTFREE 0 TABLESPACE BI_SRC
STORAGE (INITIAL 16k NEXT 16k PCTINCREASE 0)
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
ENABLE QUERY REWRITE AS
--create or replace view METRIC_DATA_FACT_VW as
select f.fac_id, f.post_date, f.metric_id, d.metric_category, (f.numerator) as numerator, (f.DENOMINATOR) as denominator,

case 
        when (f.denominator) = 0 then null
        when Metric_Scale * Metric_Up_Meaning * ((f.numerator))/((f.denominator)) < Metric_Up_Meaning * Metric_Goal_Low then 0
        when Metric_Scale * Metric_Up_Meaning * ((f.numerator))/((f.denominator)) < Metric_Up_Meaning * Metric_Goal_Med then 
                floor(4 * ((Metric_Scale * Metric_Up_Meaning * ((f.numerator))/((f.denominator)) - Metric_Up_Meaning * Metric_Goal_Low) / 
                        (Metric_Up_Meaning * Metric_Goal_Med - Metric_Up_Meaning * Metric_Goal_Low)) + 1)
        when Metric_Scale * Metric_Up_Meaning * ((f.numerator))/((f.denominator)) < Metric_Up_Meaning * Metric_Goal_High then 
                floor(5 * ((Metric_Scale * Metric_Up_Meaning * ((f.numerator))/((f.denominator)) - Metric_Up_Meaning * Metric_Goal_Med) / 
                        (Metric_Up_Meaning * Metric_Goal_High - Metric_Up_Meaning * Metric_Goal_Med)) + 5)
        else 10 end 
as CQS_Score

from (
  select *
--          fac_id,
--          post_date,
--          METRIC_ID
          
--          sum(case when m.metric_position = 'numerator' then nvl(metric_value,0) else 0 end) as numerator,
--         sum(case when m.metric_position = 'denominator' then nvl(metric_value,0) else 0 end) as denominator
  from 
  (
          select to_number(subject_id) as fac_id, post_date, ml.metric_id, metric_position, metric_value
          from metric_data_fact m inner join (
                  select metric_id, METRIC_part_id, metric_position
                  from (				
                          select m1.metric_id, m2.metric_id as METRIC_part_id, 'numerator' as metric_position
                          FROM METRIC_DATA_DIM m1 left join METRIC_DATA_DIM m2
                          on m1.metric_numerator = m2.metric_cd
                          where m1.metric_calc = 'Rate'
                          --  and (cqs_flag = 1 or us_flag = 1)
                          union 
                          select m1.metric_id, m2.metric_id as METRIC_part_id, 'denominator' as metric_position
                          FROM METRIC_DATA_DIM m1 left join METRIC_DATA_DIM m2
                          on m1.metric_denominator = m2.metric_cd
                          where m1.metric_calc = 'Rate'
                          and m1.metric_denominator <> '1'
                          --  and (cqs_flag = 1 or us_flag = 1)
                  ) mr
          ) ml on m.METRIC_id = ml.metric_part_id
  ) 
  pivot
  (
        sum(METRIC_VALUE)
        for metric_position in ('numerator' as "NUMERATOR", 'denominator' as "DENOMINATOR")
  )
--  group by         
--          m.fac_id,
--          m.post_date, 
--          m.METRIC_ID
          
          ) f inner join
        (select 
          metric_id,
          metric_category,
          Metric_Scale, 
          case when Metric_Up_Meaning = 'good' then 1 else -1 end as Metric_Up_Meaning, 
          Metric_Goal_Low, Metric_Goal_Med, Metric_Goal_High
          from metric_data_dim
         ) d         
on f.metric_id = d.metric_id


CREATE INDEX MDF_VW_FAC_ID_IDX 
   ON METRIC_DATA_FACT_MV (FAC_ID);
   
CREATE INDEX MDF_VW_POST_DATE_IDX 
   ON METRIC_DATA_FACT_MV (POST_DATE);

CREATE INDEX MDF_VW_METRIC_CATEGORY_IDX 
   ON METRIC_DATA_FACT_MV (METRIC_CATEGORY);

CREATE INDEX MDF_VW_METRIC_ID_IDX 
   ON METRIC_DATA_FACT_MV (METRIC_ID);


DROP VIEW METRIC_DATA_FACT_VW
DROP SYNONYM METRIC_DATA_FACT_S

DROP VIEW METRIC_DATA_FACT_VW

CREATE VIEW METRIC_DATA_FACT_VW as
SELECT f.*, m.metric_cd, m.metric_numerator, m.metric_denominator 
from METRIC_DATA_FACT_MV f inner join METRIC_DATA_DIM m
        on f.metric_id = m.metric_id

--CREATE SYNONYM BI_SRC.METRiC_DATA_FACT_S FOR BI_SRC.METRiC_DATA_FACT_VW;
GRANT SELECT ON VIEW METRIC_DATA_FACT_VW to PUBLIC
   

-- group by f.fac_id, f.post_date, f.metric_id, d.metric_category, Metric_Scale, Metric_Up_Meaning, Metric_Goal_Low, Metric_Goal_Med, Metric_Goal_High


drop view FAC_FACT_FLAT_VW;
create view FAC_FACT_FLAT_VW as
select *
from (
  select *
  from 
  (
          select to_number(subject_id) as fac_id, trunc(post_date, 'MONTH') as post_date, metric_cd, metric_value
          from metric_data_fact
  ) 
  pivot
  (
        sum(metric_value)
        for metric_cd in (
        'OBSERVATION HOSP. ADMISSION (<24 HRS)' as "OBS HOSP. ADMISSION<24 HRS",
        'PHOS_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_HHD' as "PHOS_NOT_TEST_CURR_MO_1_D_HHD",
        'PHOS_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_IHD' as "PHOS_NOT_TEST_CURR_MO_1_D_IHD",
        'PHOS_NOT_TESTED_CURR_MONTH_1_NUMERATOR_HHD' as "PHOS_NOT_TEST_CURR_MO_1_N_HHD",
        'PHOS_NOT_TESTED_CURR_MONTH_1_NUMERATOR_IHD' as "PHOS_NOT_TEST_CURR_MO_1_N_IHD",
        'HGB_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_HHD' as "HGB_NOT_TEST_CURR_MO_1_D_HHD",
        'HGB_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_IHD' as "HGB_NOT_TEST_CURR_MO_1_D_IHD",
        'HGB_NOT_TESTED_CURR_MONTH_1_NUMERATOR_HHD' as "HGB_NOT_TEST_CURR_MO_1_N_HHD",
        'HGB_NOT_TESTED_CURR_MONTH_1_NUMERATOR_IHD' as "HGB_NOT_TEST_CURR_MO_1_N_IHD",
        'PHOS_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_PD' as "PHOS_NOT_TEST_CURR_MO_1_D_PD",
        'PHOS_NOT_TESTED_CURR_MONTH_1_NUMERATOR_PD' as "PHOS_NOT_TEST_CURR_MO_1_N_PD",
        'HGB_NOT_TESTED_CURR_MONTH_1_DENOMINATOR_PD' as "HGB_NOT_TEST_CURR_MO_1_D_PD",
        'HGB_NOT_TESTED_CURR_MONTH_1_NUMERATOR_PD' as "HGB_NOT_TEST_CURR_MO_1_N_PD",
        'N_ELIG_PT_MON_ADULT_HD_KTV12_HHD' as "ELIG_PT_MO_ADULT_HD_KTV12_HHD",
        'N_ELIG_PT_MON_ADULT_HD_KTV12_IHD' as "ELIG_PT_MO_ADULT_HD_KTV12_IHD",
        'N_ELIG_PT_MON_PEDIA_HD_KTV12_IHD' as "ELIG_PT_MO_PEDIA_HD_KTV12_IHD",
                
        'N_ELIGIBLE_PT_MON_HD_KTV12_HHD' as "N_ELIGIBLE_PT_MON_HD_KTV12_HHD",
        'N_ELIGIBLE_PT_MON_HD_KTV12_IHD' as "N_ELIGIBLE_PT_MON_HD_KTV12_IHD",
        'N_ELIGIBLE_PT_MON_HYPERCAL_HHD' as "N_ELIGIBLE_PT_MON_HYPERCAL_HHD",
        'N_ELIGIBLE_PT_MON_HYPERCAL_IHD' as "N_ELIGIBLE_PT_MON_HYPERCAL_IHD",
        'N_ELIGIBLE_PT_MON_HYPERCAL_PD' as "N_ELIGIBLE_PT_MON_HYPERCAL_PD",
        'N_ELIGIBLE_PT_MON_PD_KTV17_PD' as "N_ELIGIBLE_PT_MON_PD_KTV17_PD",
        'N_ELIG_PT_ADULT_HD_KTV12_HHD' as "N_ELIG_PT_ADULT_HD_KTV12_HHD",
        'N_ELIG_PT_ADULT_HD_KTV12_IHD' as "N_ELIG_PT_ADULT_HD_KTV12_IHD",
        'N_ELIG_PT_PEDIA_HD_KTV12_IHD' as "N_ELIG_PT_PEDIA_HD_KTV12_IHD",
        'N_PT_MON_KTV999_ELIGIBLE_HHD' as "N_PT_MON_KTV999_ELIGIBLE_HHD",
        'N_PT_MON_KTV999_ELIGIBLE_IHD' as "N_PT_MON_KTV999_ELIGIBLE_IHD",
        'V17_CALCIUM_UNCORR_DR3_DENOMINATOR_IHD' as "V17_CALCIUM_UNCORR_DR3_D_IHD",
        'V17_CALCIUM_UNCORR_DR3_NUMERATOR_IHD' as "V17_CALCIUM_UNCORR_DR3_N_IHD",
        'CAHPS_Rate_Facility_Staff_DENOMINATOR' as "CAHPS_Rate_Facility_Staff_D",
        'CAHPS_Rate_Facility_Staff_NUMERATOR' as "CAHPS_Rate_Facility_Staff_N",
        'CALCIUM_UNCORR_3B_DR3_DENOMINATOR_HHD' as "CALCIUM_UNCORR_3B_DR3_D_HHD",
        'CALCIUM_UNCORR_3B_DR3_NUMERATOR_HHD' as "CALCIUM_UNCORR_3B_DR3_N_HHD",
        'N_ELIGIBLE_PT_MON_HYPERCAL_' as "N_ELIGIBLE_PT_MON_HYPERCAL_",
        'N_PT_MON_ADULT_HD_KTV12_HHD' as "N_PT_MON_ADULT_HD_KTV12_HHD",
        'N_PT_MON_ADULT_HD_KTV12_IHD' as "N_PT_MON_ADULT_HD_KTV12_IHD",
        'N_PT_MON_KTV999_ELIGIBLE_PD' as "N_PT_MON_KTV999_ELIGIBLE_PD",
        'N_PT_MON_PEDIA_HD_KTV12_IHD' as "N_PT_MON_PEDIA_HD_KTV12_IHD",
        'CALCIUM_UNCORR_3B_DR4_DENOMINATOR_PD' as "CALCIUM_UNCORR_3B_DR4_D_PD",
        'CALCIUM_UNCORR_3B_DR4_NUMERATOR_PD' as "CALCIUM_UNCORR_3B_DR4_N_PD",
        'N_ELIGIBLE_PT_HD_KTV12_HHD' as "N_ELIGIBLE_PT_HD_KTV12_HHD",
        'N_ELIGIBLE_PT_HD_KTV12_IHD' as "N_ELIGIBLE_PT_HD_KTV12_IHD",
        'N_ELIGIBLE_PT_HYPERCAL_HHD' as "N_ELIGIBLE_PT_HYPERCAL_HHD",
        'N_ELIGIBLE_PT_HYPERCAL_IHD' as "N_ELIGIBLE_PT_HYPERCAL_IHD",
        'N_ELIGIBLE_PT_MON_CATH_HHD' as "N_ELIGIBLE_PT_MON_CATH_HHD",
        'N_ELIGIBLE_PT_MON_CATH_IHD' as "N_ELIGIBLE_PT_MON_CATH_IHD",
        'N_ELIGIBLE_PT_MON_FIST_HHD' as "N_ELIGIBLE_PT_MON_FIST_HHD",
        'N_ELIGIBLE_PT_MON_FIST_IHD' as "N_ELIGIBLE_PT_MON_FIST_IHD",
        'N_PT_MON_HYPERCAL_10_2_HHD' as "N_PT_MON_HYPERCAL_10_2_HHD",
        'N_PT_MON_HYPERCAL_10_2_IHD' as "N_PT_MON_HYPERCAL_10_2_IHD",
        'HOSPITALIZATION (>24 HRS)' as "HOSPITALIZATION (>24 HRS)",
        'N_ELIGIBLE_PT_HD_KTV17_PD' as "N_ELIGIBLE_PT_HD_KTV17_PD",
        'N_ELIGIBLE_PT_HYPERCAL_PD' as "N_ELIGIBLE_PT_HYPERCAL_PD",
        'N_PT_MON_HYPERCAL_10_2_PD' as "N_PT_MON_HYPERCAL_10_2_PD",
        'SPKTV_OR_WSTDKTV_4_DENOMINATOR_IHD' as "SPKTV_OR_WSTDKTV_4_D_IHD",
        'SPKTV_OR_WSTDKTV_4_NUMERATOR_IHD' as "SPKTV_OR_WSTDKTV_4_N_IHD",
        'WKTV_NOT_TESTED_QTR_DENOMINATOR_PD' as "WKTV_NOT_TESTED_QTR_D_PD",
        'WKTV_NOT_TESTED_QTR_NUMERATOR_PD' as "WKTV_NOT_TESTED_QTR_N_PD",
        'N_ELIGIBLE_PT_HYPERCAL_' as "N_ELIGIBLE_PT_HYPERCAL_",
        'N_ELIGIBLE_PT_MON_CATH_' as "N_ELIGIBLE_PT_MON_CATH_",
        'N_ELIGIBLE_PT_MON_FIST_' as "N_ELIGIBLE_PT_MON_FIST_",
        'N_PT_MON_HYPERCAL_10_2_' as "N_PT_MON_HYPERCAL_10_2_",
        'V17_WSTDKTV_3_DR2_DENOMINATOR_HHD' as "V17_WSTDKTV_3_DR2_D_HHD",
        'V17_WSTDKTV_3_DR2_NUMERATOR_HHD' as "V17_WSTDKTV_3_DR2_N_HHD",
        'N_ELIGIBLE_PT_CATH_HHD' as "N_ELIGIBLE_PT_CATH_HHD",
        'N_ELIGIBLE_PT_CATH_IHD' as "N_ELIGIBLE_PT_CATH_IHD",
        'N_ELIGIBLE_PT_FIST_HHD' as "N_ELIGIBLE_PT_FIST_HHD",
        'N_ELIGIBLE_PT_FIST_IHD' as "N_ELIGIBLE_PT_FIST_IHD",
        'CAHPS_Rate_Facility_DENOMINATOR' as "CAHPS_Rate_Facility_D",
        'CAHPS_Rate_Facility_NUMERATOR' as "CAHPS_Rate_Facility_N",
        'FACILITY_CENSUS_PD_PD' as "FACILITY_CENSUS_PD_PD",
        'N_PT_MON_HD_KTV12_HHD' as "N_PT_MON_HD_KTV12_HHD",
        'N_PT_MON_HD_KTV12_IHD' as "N_PT_MON_HD_KTV12_IHD",
        'V17_CATHETERS_3_DENOMINATOR_HHD' as "V17_CATHETERS_3_D_HHD",
        'V17_CATHETERS_3_DENOMINATOR_IHD' as "V17_CATHETERS_3_D_IHD",
        'V17_CATHETERS_3_NUMERATOR_HHD' as "V17_CATHETERS_3_N_HHD",
        'V17_CATHETERS_3_NUMERATOR_IHD' as "V17_CATHETERS_3_N_IHD",
        'CAHPS_Patient_Info_DENOMINATOR' as "CAHPS_Patient_Info_D",
        'CAHPS_Patient_Info_NUMERATOR' as "CAHPS_Patient_Info_N",
        'CAHPS_Quality_Care_DENOMINATOR' as "CAHPS_Quality_Care_D",
        'CAHPS_Quality_Care_NUMERATOR' as "CAHPS_Quality_Care_N",
        'N_PT_MON_PD_KTV17_PD' as "N_PT_MON_PD_KTV17_PD",
        'PATIENT_UFR_LS13_MON' as "PATIENT_UFR_LS13_MON",
        'FACILITY_CENSUS_HHD' as "FACILITY_CENSUS_HHD",
        'FACILITY_CENSUS_IHD' as "FACILITY_CENSUS_IHD",
        'NHSN_Infection_Hosp' as "NHSN_Infection_Hosp",
        'N_ELIGIBLE_PT_CATH_' as "N_ELIGIBLE_PT_CATH_",
        'N_ELIGIBLE_PT_FIST_' as "N_ELIGIBLE_PT_FIST_",
        'N_PT_MON_CATH90_HHD' as "N_PT_MON_CATH90_HHD",
        'N_PT_MON_CATH90_IHD' as "N_PT_MON_CATH90_IHD",
        'N_PT_MON_KTV999_HHD' as "N_PT_MON_KTV999_HHD",
        'N_PT_MON_KTV999_IHD' as "N_PT_MON_KTV999_IHD",
        'PERITONITIS_24_DENOMINATOR_PD' as "PERITONITIS_24_D_PD",
        'PERITONITIS_24_NUMERATOR_PD' as "PERITONITIS_24_N_PD",
        'FACILITY_CENSUS_PD' as "FACILITY_CENSUS_PD",
        'HEMOGLOBIN_3_DENOMINATOR_HHD' as "HEMOGLOBIN_3_D_HHD",
        'HEMOGLOBIN_3_DENOMINATOR_IHD' as "HEMOGLOBIN_3_D_IHD",
        'HEMOGLOBIN_3_NUMERATOR_HHD' as "HEMOGLOBIN_3_N_HHD",
        'HEMOGLOBIN_3_NUMERATOR_IHD' as "HEMOGLOBIN_3_N_IHD",
        'N_PT_MON_KTV999_PD' as "N_PT_MON_KTV999_PD",
        'CAHPS_Comm_Care_DENOMINATOR' as "CAHPS_Comm_Care_D",
        'CAHPS_Comm_Care_NUMERATOR' as "CAHPS_Comm_Care_N",
        'HEMOGLOBIN_3_DENOMINATOR_PD' as "HEMOGLOBIN_3_D_PD",
        'HEMOGLOBIN_3_NUMERATOR_PD' as "HEMOGLOBIN_3_N_PD",
        'N_PT_MON_FIST_HHD' as "N_PT_MON_FIST_HHD",
        'N_PT_MON_FIST_IHD' as "N_PT_MON_FIST_IHD",
        'CAHPS_Rate_MD__DENOMINATOR' as "CAHPS_Rate_MD__D",
        'CAHPS_Rate_MD__NUMERATOR' as "CAHPS_Rate_MD__N",
        'MRX_REV_DENO_HHD' as "MRX_REV_DENO_HHD",
        'MRX_REV_DENO_IHD' as "MRX_REV_DENO_IHD",
        'N_PT_MON_CATH90_' as "N_PT_MON_CATH90_",
        'PATIENT_UFR_LS13' as "PATIENT_UFR_LS13",
        'TOTAL_TREATMENTS' as "TOTAL_TREATMENTS",
        'V17_WKT_V_4_DENOMINATOR_PD' as "V17_WKT_V_4_D_PD",
        'V17_WKT_V_4_NUMERATOR_PD' as "V17_WKT_V_4_N_PD",
        'WKT_V_3_DR1_DENOMINATOR_PD' as "WKT_V_3_DR1_D_PD",
        'WKT_V_3_DR1_NUMERATOR_PD' as "WKT_V_3_DR1_N_PD",
        'WSTDKT_V_3_DENOMINATOR_HHD' as "WSTDKT_V_3_D_HHD",
        'WSTDKT_V_3_NUMERATOR_HHD' as "WSTDKT_V_3_N_HHD",
        'ALBUMIN_3_DENOMINATOR_HHD' as "ALBUMIN_3_D_HHD",
        'ALBUMIN_3_DENOMINATOR_IHD' as "ALBUMIN_3_D_IHD",
        'ALBUMIN_3_NUMERATOR_HHD' as "ALBUMIN_3_N_HHD",
        'ALBUMIN_3_NUMERATOR_IHD' as "ALBUMIN_3_N_IHD",
        'INCIDENTS_PD_PD' as "INCIDENTS_PD_PD",
        'MRX_REV_DENO_PD' as "MRX_REV_DENO_PD",
        'MRX_REV_NUM_HHD' as "MRX_REV_NUM_HHD",
        'MRX_REV_NUM_IHD' as "MRX_REV_NUM_IHD",
        'N_PT_CATH90_HHD' as "N_PT_CATH90_HHD",
        'N_PT_CATH90_IHD' as "N_PT_CATH90_IHD",
        'UFR13_1MO_DENOMINATOR_IHD' as "UFR13_1MO_D_IHD",
        'UFR13_1MO_NUMERATOR_IHD' as "UFR13_1MO_N_IHD",
        'UFR13_3MO_DENOMINATOR_IHD' as "UFR13_3MO_D_IHD",
        'UFR13_3MO_NUMERATOR_IHD' as "UFR13_3MO_N_IHD",
        'V17_CMB_3_DENOMINATOR_HHD' as "V17_CMB_3_D_HHD",
        'V17_CMB_3_DENOMINATOR_IHD' as "V17_CMB_3_D_IHD",
        'V17_CMB_3_NUMERATOR_HHD' as "V17_CMB_3_N_HHD",
        'V17_CMB_3_NUMERATOR_IHD' as "V17_CMB_3_N_IHD",
        'ALBUMIN_3_DENOMINATOR_PD' as "ALBUMIN_3_D_PD",
        'ALBUMIN_3_NUMERATOR_PD' as "ALBUMIN_3_N_PD",
        'MRX_REV_NUM_PD' as "MRX_REV_NUM_PD",
        'N_PT_MON_FIST_' as "N_PT_MON_FIST_",
        'V17_CMB_3_DENOMINATOR_PD' as "V17_CMB_3_D_PD",
        'V17_CMB_3_NUMERATOR_PD' as "V17_CMB_3_N_PD",
        'N_PT_FIST_HHD' as "N_PT_FIST_HHD",
        'N_PT_FIST_IHD' as "N_PT_FIST_IHD",
        'SPKTV_3_DENOMINATOR_IHD' as "SPKTV_3_D_IHD",
        'SPKTV_3_NUMERATOR_IHD' as "SPKTV_3_N_IHD",
        'TotalPatients' as "TotalPatients",
        '728METRIC_14' as "728METRIC_14",
        '742METRIC_14' as "742METRIC_14",
        '756METRIC_14' as "756METRIC_14",
        'CLINIC_COUNT' as "CLINIC_COUNT",
        'INCIDENTS_PD' as "INCIDENTS_PD",
        'N_PT_CATH90_' as "N_PT_CATH90_",
        '718METRIC_4' as "718METRIC_4",
        '732METRIC_4' as "732METRIC_4",
        '746METRIC_4' as "746METRIC_4",
        'YellowPtCnt' as "YellowPtCnt",
        'FTCK_DENOMINATOR_IHD' as "FTCK_D_IHD",
        'FTCK_NUMERATOR_IHD' as "FTCK_N_IHD",
        'GreenPtCnt' as "GreenPtCnt",
        'HEPB_DENOMINATOR_HHD' as "HEPB_D_HHD",
        'HEPB_DENOMINATOR_IHD' as "HEPB_D_IHD",
        'HEPB_NUMERATOR_HHD' as "HEPB_N_HHD",
        'HEPB_NUMERATOR_IHD' as "HEPB_N_IHD",
        'N_PT_FIST_' as "N_PT_FIST_",
        'TX_PWV_GT1' as "TX_PWV_GT1",
        'FLU_DENOMINATOR_HHD' as "FLU_D_HHD",
        'FLU_DENOMINATOR_IHD' as "FLU_D_IHD",
        'FLU_NUMERATOR_HHD' as "FLU_N_HHD",
        'FLU_NUMERATOR_IHD' as "FLU_N_IHD",
        'HEPB_DENOMINATOR_PD' as "HEPB_D_PD",
        'HEPB_NUMERATOR_PD' as "HEPB_N_PD",
        'FLU_DENOMINATOR_PD' as "FLU_D_PD",
        'FLU_NUMERATOR_PD' as "FLU_N_PD",
        'RedPtCnt' as "RedPtCnt",
        'BSI_HHD' as "BSI_HHD",
        'BSI_IHD' as "BSI_IHD",
        'NPS_DENOMINATOR' as "NPS_D",
        'NPS_NUMERATOR' as "NPS_N"
        )
  )         
) f 
          
          


--select distinct '''' || metric_cd || ''' as "' || REPLACE(REPLACE(metric_cd,'DENOMINATOR','D'),'NUMERATOR','N') || '",' as col,
--length(REPLACE(REPLACE(metric_cd,'DENOMINATOR','D'),'NUMERATOR','N')) l
--from metric_data_fact


/*
        p_filter_global_id VARCHAR2, - 'facility_id (padded with zeros to make 6 characters), or global_id of area, region, group and division'
        p_filter_hierarchy_level INT, - '0=facility, 1=area, 2=region, 3=group, 4=division'
        p_hierarchy_name VARCHAR2, 'organizational, ESCO'
        p_start_date VARCHAR2, 'YYYY-MM-DD format'
        p_end_date VARCHAR2, 'YYYY-MM-DD format'
        p_grouping_hierarchy_level INT, - '0=facility, 1=area, 2=region, 3=group, 4=division'
        p_modality VARCHAR2, - 'IHD, HHD, PD'
        p_grouping_metric_level INT - '1=Total, 2=category, 3=group, 4=metric/modality'
*/


select metric_id, metric_label
from metric_data_dim
where cqs_flag = 1
ORDER BY metric_label


  select *
  from TABLE(get_metricDataObjects(
  N'100005', 
  4, 
  'organizational', 
  to_char(TO_DATE('2016-11-01', 'YYYY-MM-DD'), 'YYYY-MM-DD'),
  to_char(TO_DATE('2017-01-01', 'YYYY-MM-DD'), 'YYYY-MM-DD'),
  0, 
  N'''IHD'',''PD''', 
  1,
  ''))



select t.*, DENSE_RANK() OVER (ORDER BY metric_value) as rank
from (TABLE(get_metricDataObjects( '100005', 4, 'organizational', '2016-12-01', '2016-12-31', 1, '''IHD'',''PD''', 4, '' ))) t
where metric_id = 413

select *
from TABLE(get_metricDataObjects('008659', 0, 'organizational', '2016-12-01', '2016-12-31', 0, '''IHD''', 4, '
        {
          "rows": [
            {
              "id": "310",
              "name": "Albumin IHD",
              "cqs_flag":0,
              "current": [
                24.64,
                34.82,
                45,
                10
              ],
              "new": [
                30.85153846153846,
                45.93538461538461,
                62.980769230769226,
                25
              ]
            },
            {
              "id": "331",
              "name": "Foot Check IHD",
              "current": [
                50,
                75,
                100,
                10
              ],
              "new": [
                69.50470219435736,
                87.47021943573668,
                100,
                10
              ]
            }
          ]
        }
        ')
)

select * 
from (
select post_date, hierarchy_label, hierarchy_label, metric_value, DENSE_RANK() OVER (ORDER BY metric_value) as rank -- metric_value
from TABLE(get_metricDataObjects('107696,100005,106917,110893,106943', 4, 'organizational', '2016-12-01','2016-12-31',3, '''IHD'',''HHD'',''PD''', 4)) tf
where metric_label = 'spKt/V 1.4 IHD'
) tf1
where rank <= 10


select * 
from FAC_DIAL_HIERARCHY f 
where f."level" = ''facility''
and hierlevel(f.bit_id, ' || p_filter_hierarchy_level || ', 6) in (' || p_filter_global_id || ')
and hierarchy = ''' || p_hierarchy_name || '''

